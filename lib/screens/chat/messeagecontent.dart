import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';

// Suppose equation of straight line has format: y = kx + c and Straight line goes through 2 points: p1 & p2
class LineEquationHelper {
  static double getSlopeK(Point pA, Point pB) {
    return (pA.x == pB.x) ? 0.0 : (pB.y - pA.y) / (pB.x - pA.x);
  }

  static double getInterceptC(Point pA, Point pB) {
    return pA.y - getSlopeK(pA, pB) * pA.x;
  }
}

// Suppose the equation of circle with center I(xI, yI) and radius r is: (x - xI)^2 + (y - yI)^2 = r^2
class CircleEquationHelper {
  // The x coordinate of the interception of circle and straight line that mentioned about is the root of function:
  // x^2 + bx + c = 0 with ...

  static getCoefficientB(Point pA, Point pB, Point pI, double radius) {
    double scopeK = LineEquationHelper.getSlopeK(pA, pB);
    return 2 *
        (scopeK * (LineEquationHelper.getInterceptC(pA, pB) - pI.y) - pI.x) /
        (1 + scopeK * scopeK);
  }

  static getCoefficientC(Point pA, Point pB, Point pI, double radius) {
    double scopeK = LineEquationHelper.getSlopeK(pA, pB);
    double interceptC = LineEquationHelper.getInterceptC(pA, pB);

    return (pI.x * pI.x +
            interceptC * interceptC -
            2 * interceptC * pI.y +
            pI.y * pI.y -
            radius * radius) /
        (1 + scopeK * scopeK);
  }

  static isInterceptionEquationHasRoot(
      Point pA, Point pB, Point pI, double radius) {
    double coefficientB = getCoefficientB(pA, pB, pI, radius);
    double coefficientC = getCoefficientC(pA, pB, pI, radius);

    return coefficientB * coefficientB - 4 * coefficientC >= 0;
  }

  static _intersectionWhen2PointsEqualX(
      Point pA, Point pB, Point pI, double radius) {
    // Condition: pI not between line segment AB.
    if (pA.y == pB.y) {
      return pI;
    } else {
      double maxY, minY;
      if (pA.y > pB.y) {
        maxY = pA.y;
        minY = pB.y;
      } else {
        maxY = pB.y;
        minY = pA.y;
      }

      if (pI.y >= maxY) {
        return Point(pI.x, pI.y - radius);
      }
      if (pI.y <= minY) {
        return Point(pI.x, pI.y + radius);
      }
    }
  }

  static _intersectionWhen2PointsEqualY(
      Point pA, Point pB, Point pI, double radius) {
    if (pA.x == pB.x) {
      return pI;
    } else {
      double maxX, minX;
      if (pA.x > pB.x) {
        maxX = pA.x;
        minX = pB.x;
      } else {
        maxX = pB.x;
        minX = pA.x;
      }

      if (pI.x >= maxX) {
        return Point(pI.x - radius, pI.y);
      }
      if (pI.x <= minX) {
        return Point(pI.x + radius, pI.y);
      }
    }
  }

  static validIntersectionPointStraightLineAndCircle(
      Point pA, Point pB, Point pI, double radius) {
    if (pA.x == pB.x) {
      return _intersectionWhen2PointsEqualX(pA, pB, pI, radius);
    }
    if (pA.y == pB.y) {
      return _intersectionWhen2PointsEqualY(pA, pB, pI, radius);
    }
    // x^2 + bx + c = 0 has 2 roots: (-b +- sqrt(delta))/2 with delta = b^2 - 4*c (a = 1)
    double coefficientB = getCoefficientB(pA, pB, pI, radius);
    double coefficientC = getCoefficientC(pA, pB, pI, radius);

    double sqrtDelta = sqrt(coefficientB * coefficientB - 4 * coefficientC);
    double x1 = (coefficientB * (-1) - sqrtDelta) / 2.0;
    // Check whether x1 between 2 points pA & pB
    if ((pA.x > pB.x && (x1 > pB.x && x1 < pA.x)) ||
        (pA.x < pB.x && (x1 > pA.x && x1 < pB.x))) {
      return Point(
          x1,
          LineEquationHelper.getSlopeK(pA, pB) * x1 +
              LineEquationHelper.getInterceptC(pA, pB));
    }

    x1 = (coefficientB * (-1) + sqrtDelta) / 2.0;
    return Point(
        x1,
        LineEquationHelper.getSlopeK(pA, pB) * x1 +
            LineEquationHelper.getInterceptC(pA, pB));
  }
}

enum ARROW_TYPE { LEFT_ARROW, RIGHT_ARROW }
enum STATUS_ICON_TYPE { PREFIX_ICON, SUFFIX_ICON }
enum MESSAGE_STATUS_POSITION { LEFT, RIGHT }

class MessageShapeClipper extends CustomClipper<Path> {
  final double radius;

  final double arrowWidth; // Arrow
  final double arrowHeight; // Arrow

  final ARROW_TYPE arrowType;

  MessageShapeClipper(
      {this.radius = 20.0,
      this.arrowWidth = 10.0,
      this.arrowHeight = 10.0,
      this.arrowType = ARROW_TYPE.LEFT_ARROW});

  List<Point> _convertContainerSizeToListPoints(double w, double h) {
    List<Point> pts = List();
    switch (arrowType) {
      case ARROW_TYPE.LEFT_ARROW:
        pts.add(Point(0.0, h));
        pts.add(Point(this.arrowWidth, h - this.arrowHeight));
        pts.add(Point(this.arrowWidth, 0.0));
        pts.add(Point(w, 0.0));
        pts.add(Point(w, h));
        break;
      default:
        pts.add(Point(w, h));
        pts.add(Point(w - this.arrowWidth, h - this.arrowHeight));
        pts.add(Point(w - this.arrowWidth, 0.0));
        pts.add(Point(0.0, 0.0));
        pts.add(Point(0.0, h));
        break;
    }
    return pts;
  }

  @override
  Path getClip(Size size) {
    Path path = Path();
    List<Point> pts =
        _convertContainerSizeToListPoints(size.width, size.height);

    pts.add(pts[0]);
    Point startPaintPoint = pts[0];
    Point endPaintPoint = pts[pts.length - 1];

    path.moveTo(startPaintPoint.x, startPaintPoint.y);
    path.lineTo(pts[1].x, pts[1].y);

    for (int i = 1; i <= 3; i++) {
      Point interP1 =
          CircleEquationHelper.validIntersectionPointStraightLineAndCircle(
              pts[i], pts[i + 1], pts[i + 1], this.radius);
      Point interP2 =
          CircleEquationHelper.validIntersectionPointStraightLineAndCircle(
              pts[i + 1], pts[i + 2], pts[i + 1], this.radius);

      path.lineTo(interP1.x, interP1.y);
      path.arcToPoint(Offset(interP2.x, interP2.y),
          radius: Radius.circular(this.radius),
          clockwise: (this.arrowType == ARROW_TYPE.LEFT_ARROW) ? true : false);
    }

    path.lineTo(endPaintPoint.x, endPaintPoint.y);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

/*
*   Class helps make message image (the shape and image content of message).
*   + width: the width of image content.
*   + height: the height of image content.
*   + arrowWidth: width of the arrow at bottom corner of message shape.
*   + arrowHeight: height of the arrow at bottom corner of message shape.
*   + title: title of message.
*   + titleMessageStyle: text style for title message that mentioned above.
*   + arrowType: whether arrow is placed at left bottom or right bottom of the shape.
*
*   + msgSttBar: please read the documentation of MessageStatusBar class ^^
*   + msgSttBarPosition: whether this status bar is placed at the left or right side of message.
*/

enum MEDIA_MESSAGE_TYPE { IMAGE, VIDEO }

class MediaMessageContent extends StatelessWidget {
  final String imgUrl;
  final int location; // 0: assets, 1: gallery

  final double width;
  final double height;

  final double arrowWidth;
  final double arrowHeight;

  final String title;
  final TextStyle titleMessageStyle;

  final ARROW_TYPE arrowType;

  final MEDIA_MESSAGE_TYPE mediaMessageType;

  // Status bar
  final MESSAGE_STATUS_POSITION msgSttBarPosition;
  final MessageStatusBar msgSttBar;

  const MediaMessageContent(this.width, this.height, this.title, this.imgUrl,
      {this.location = 0,
      this.titleMessageStyle = const TextStyle(
          color: Colors.white, fontSize: 32.0, fontWeight: FontWeight.bold),
      this.arrowWidth = 10.0,
      this.arrowHeight = 10.0,
      this.msgSttBar,
      this.arrowType = ARROW_TYPE.LEFT_ARROW,
      this.msgSttBarPosition = MESSAGE_STATUS_POSITION.RIGHT,
      this.mediaMessageType = MEDIA_MESSAGE_TYPE.IMAGE})
      : assert(imgUrl != "Empty image url is invalid"),
        assert(title != null, "The title of message can not be null!"),
        assert(arrowWidth >= 10.0 && arrowHeight >= 10.0,
            "The min size of corner arrow is 10.0");

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment:
            this.msgSttBarPosition == MESSAGE_STATUS_POSITION.LEFT
                ? CrossAxisAlignment.start
                : CrossAxisAlignment.end,
        children: <Widget>[
          ClipPath(
            clipper: MessageShapeClipper(arrowType: this.arrowType),
            child: Container(
                width: this.width,
                height: this.height,
                child: Stack(
                  children: <Widget>[
                    this.location == 0
                        ? Image.asset(
                            this.imgUrl,
                            width: this.width,
                            height: this.height,
                            fit: BoxFit.cover,
                          )
                        : Image.file(
                            new File(this.imgUrl),
                            width: this.width,
                            height: this.height,
                            fit: BoxFit.cover,
                          ),
                    (this.mediaMessageType == MEDIA_MESSAGE_TYPE.IMAGE &&
                            this.title == "")
                        ? Container()
                        : _getCoverLayer(
                            this.title,
                            this.titleMessageStyle,
                            this.mediaMessageType,
                            (this.arrowType == ARROW_TYPE.LEFT_ARROW
                                    ? this.arrowWidth
                                    : 0.0) +
                                15.0,
                            15.0,
                            15.0),
                  ],
                )),
          ),
          (this.msgSttBar == null)
              ? Container()
              : this.msgSttBarPosition == MESSAGE_STATUS_POSITION.RIGHT
                  ? Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 20.0, top: 10.0),
                        child: this.msgSttBar,
                      ),
                    )
                  : Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 5.0),
                        child: this.msgSttBar,
                      ),
                    )
        ],
      ),
    );
  }

  Widget _getCoverLayer(
      String title,
      TextStyle titleTextStyle,
      MEDIA_MESSAGE_TYPE mediaMsgType,
      double paddingLeft,
      double paddingTop,
      double paddingBottom) {
    return Container(
      padding: EdgeInsets.only(
          left: paddingLeft, top: paddingTop, bottom: paddingBottom),
      child: title != ""
          ? (this.mediaMessageType == MEDIA_MESSAGE_TYPE.VIDEO
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: titleTextStyle,
                    ),
                    FloatingActionButton(
                      onPressed: () => {},
                      child: Icon(
                        Icons.play_arrow,
                        color: Colors.white,
                      ),
                      backgroundColor: const Color(0xffee8777),
                      mini: true,
                    )
                  ],
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: titleTextStyle,
                    ),
                  ],
                ))
          : Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FloatingActionButton(
                  onPressed: () => {},
                  child: Icon(
                    Icons.play_arrow,
                    color: Colors.white,
                  ),
                  backgroundColor: const Color(0xffee8777),
                  mini: true,
                )
              ],
            ),
    );
  }
}

/*
*   Class helps make message text (the shape and content of message).
*   + contentMessage: main content of message (can be displayed in multiple lines).
*   + contentMessageStyle: text style for content message that mentioned above.
*   + titleMessage: title of message.
*   + titleMessageStyle: text style for title message that mentioned above.
*   + bgColor: background for the shape of message
*   + arrowWidth: width of the arrow at bottom corner of message shape.
*   + arrowHeight: height of the arrow at bottom corner of message shape.
*   + arrowType: whether arrow is placed at left bottom or right bottom of the shape.
*
*   + msgSttBar: please read the documentation of MessageStatusBar class ^^
*   + msgSttBarPosition: whether this status bar is placed at the left or right side of message.
*/
class TextMessageContent extends StatelessWidget {
  final String contentMessage;
  final TextStyle contentMessageStyle;

  final String titleMessage;
  final TextStyle titleMessageStyle;

  final Color bgColor;

  final double arrowWidth;
  final double arrowHeight;

  final ARROW_TYPE arrowType;

  final MESSAGE_STATUS_POSITION msgSttBarPosition;
  final MessageStatusBar msgSttBar;

  TextMessageContent(this.titleMessage, this.contentMessage,
      {this.contentMessageStyle = const TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.normal,
          fontSize: 16.0,
          fontFamily: "SFProText"),
      this.titleMessageStyle = const TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
          fontFamily: "SFProText"),
      this.bgColor = const Color(0xFFEEEEEE),
      this.arrowWidth = 10.0,
      this.arrowHeight = 10.0,
      this.arrowType = ARROW_TYPE.LEFT_ARROW,
      this.msgSttBarPosition = MESSAGE_STATUS_POSITION.RIGHT,
      this.msgSttBar})
      : assert(titleMessage != null, "The title of message can not be null!"),
        assert(
            contentMessage != null, "The content of message can not be null!"),
        assert(arrowWidth >= 10 && arrowHeight >= 10,
            "The min size of corner arrow is 10.0");

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment:
            this.msgSttBarPosition == MESSAGE_STATUS_POSITION.LEFT
                ? CrossAxisAlignment.start
                : CrossAxisAlignment.end,
        children: <Widget>[
          ClipPath(
            clipper: MessageShapeClipper(arrowType: this.arrowType),
            child: Container(
                color: this.bgColor,
                constraints:
                    const BoxConstraints(maxWidth: 250.0, minHeight: 50.0),
                padding: EdgeInsets.only(
                    left: (this.arrowType == ARROW_TYPE.LEFT_ARROW
                            ? this.arrowWidth
                            : 0.0) +
                        15.0,
                    top: 15.0,
                    right: (this.arrowType == ARROW_TYPE.RIGHT_ARROW
                            ? this.arrowWidth
                            : 0.0) +
                        15.0,
                    bottom: 15.0),
                child: this.titleMessage != ""
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            this.titleMessage,
                            style: this.titleMessageStyle,
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Text(
                            this.contentMessage,
                            style: this.contentMessageStyle,
                            softWrap: true,
                          )
                        ],
                      )
                    : Text(
                        this.contentMessage,
                        style: this.contentMessageStyle,
                        softWrap: true,
                      )),
          ),
          this.msgSttBar == null
              ? Container()
              : this.msgSttBarPosition == MESSAGE_STATUS_POSITION.RIGHT
                  ? Padding(
                      padding: const EdgeInsets.only(right: 20.0, top: 5.0),
                      child: this.msgSttBar,
                    )
                  : Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 10.0),
                      child: this.msgSttBar,
                    )
        ],
      ),
    );
  }
}

/*
*   Create message status information such as: seen time,...
*   + statusMessage: The text display as main status, ex: "19:30 PM",...
*   + statusMessageStyle: style for text that mentioned above.
*   + statusIconType: enum represents for whether icon is placed at before or after status message.
*   + icon: Icon that provide some extra information.
*/
class MessageStatusBar extends StatelessWidget {
  final String statusMessage;
  final TextStyle statusMessageStyle;
  final STATUS_ICON_TYPE statusIconType;
  final Icon icon;

  MessageStatusBar(this.statusMessage,
      {this.icon,
      this.statusIconType = STATUS_ICON_TYPE.SUFFIX_ICON,
      this.statusMessageStyle = const TextStyle(
          color: Colors.black, fontWeight: FontWeight.normal, fontSize: 14.0)})
      : assert(statusMessage != "", "This status message should not be empty.");

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          this.icon != null &&
                  this.statusIconType == STATUS_ICON_TYPE.PREFIX_ICON
              ? Container(
                  margin: const EdgeInsets.only(right: 5.0),
                  child: this.icon,
                )
              : Container(),
          Text(
            this.statusMessage,
            style: this.statusMessageStyle,
          ),
          this.icon != null &&
                  this.statusIconType == STATUS_ICON_TYPE.SUFFIX_ICON
              ? Container(
                  margin: const EdgeInsets.only(left: 5.0),
                  child: this.icon,
                )
              : Container(),
        ],
      ),
    );
  }
}
