import 'dart:io';
import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dacs/data/images.dart';
import 'package:dacs/screens/chat/messeagecontent.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_emoji/flutter_emoji.dart';

typedef StringToVoidCallback = void Function(String);
typedef FileToVoidCallback = void Function(File);

final List<String> relyTextMsg = [
  "Phòng đẹp",
  "Vâng tôi muốn thuê phòng này",
  "Tôi thích phòng này",
  "Tạm biệt!",
];

final List<String> replyImageMsg = [
  "Wow nó rất đẹp. :yum: :yum: :yum:",
  "Nhìn rộng rãi thoáng mát. :yum: :yum: :yum:",
];

final _rnd = new Random();
int _generateValue(int min, int max) => min + _rnd.nextInt(max - min);

class ChatScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ChatScreenState();
  }
}

class ChatScreenState extends State<ChatScreen> {
  final TextEditingController textEditingController =
      new TextEditingController();
  final FocusNode _fn = new FocusNode();

  List<Message> messages = new List();

  @override
  void initState() {
    super.initState();
    messages.add(new TextMessage("Chào Huy ?", "20:01 pm",
        msgStatus: MESSAGE_STATUS.NONE));
    messages.add(new ImageMessage(Images.imgItemRoom, "20:12 pm",
        msgStatus: MESSAGE_STATUS.INFO));
    messages.add(new TextMessage("Chào bạn!", "20:10 pm",
        msgType: MESSAGE_TYPE.MESSAGE_SENDER,
        msgStatus: MESSAGE_STATUS.SEEN_ALL));

    messages.add(new TextMessage(
        (EmojiParser()).emojify("Phòng đẹp mình muốn thuê! :yum: :yum: :yum:"),
        "20:16pm"));

    messages.add(new TextMessage("Yeah!", "20:18pm",
        msgType: MESSAGE_TYPE.MESSAGE_SENDER, msgStatus: MESSAGE_STATUS.SEEN));
  }

  void _handleAutoReplyTextMsg() async {
    await Future.delayed(const Duration(seconds: 1));
    var rndIdx = _generateValue(0, 4);
    messages.add(new TextMessage("" + relyTextMsg[rndIdx], _getCurrentHour(),
        msgType: MESSAGE_TYPE.MESSAGE_RECEIVER,
        msgStatus: MESSAGE_STATUS.NONE));
    setState(() {});
  }

  void _handleAutoReplyImgMsg() async {
    await Future.delayed(const Duration(seconds: 1));
    var rndIdx = _generateValue(0, 2);
    messages.add(new TextMessage(
        (EmojiParser()).emojify(replyImageMsg[rndIdx]), _getCurrentHour(),
        msgType: MESSAGE_TYPE.MESSAGE_RECEIVER,
        msgStatus: MESSAGE_STATUS.NONE));
    setState(() {});
  }

  void _handleTextSubmit(String txtInput) {
    messages.add(new TextMessage(txtInput, _getCurrentHour(),
        msgType: MESSAGE_TYPE.MESSAGE_SENDER,
        msgStatus: MESSAGE_STATUS.SEEN_ALL));
    setState(() {});
    _handleAutoReplyTextMsg();
  }

  void _handleImageSelect(File image) {
    messages.add(new ImageMessage(image.path, _getCurrentHour(),
        imgLocation: IMAGE_LOCATION.GALLERY,
        msgType: MESSAGE_TYPE.MESSAGE_SENDER,
        msgStatus: MESSAGE_STATUS.NONE));
    setState(() {});
    _handleAutoReplyImgMsg();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // automaticallyImplyLeading: false,
        titleSpacing: 0.0,
        title: Row(
          children: <Widget>[
            CircleAvatar(
              child: Text('H'),
              backgroundColor: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Text('Huy kute')
          ],
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              // launch("tel:${widget.room.phone}");
            },
            icon: Icon(
              Icons.phone,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: GestureDetector(
        child: buildMainScreen(context, this.messages, _fn,
            this.textEditingController, _handleTextSubmit, _handleImageSelect),
        onTap: () {
          if (_fn.hasFocus) {
            _fn.unfocus();
          }
        },
      ),
    );
  }
}

Widget buildMainScreen(
    BuildContext ctx,
    List<Message> messages,
    FocusNode fn,
    TextEditingController txtCtl,
    StringToVoidCallback txtInputCb,
    FileToVoidCallback imgInputCb) {
  return WillPopScope(
      child: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(child: buildListMessage(messages)),
              buildInputTextBar(fn, txtCtl, txtInputCb, imgInputCb)
            ],
          )
        ],
      ),
      onWillPop: () {
        Navigator.pop(ctx);
        return Future.value(false);
      });
}

Widget buildInputTextBar(FocusNode fn, TextEditingController ctrl,
    StringToVoidCallback txtInputCb, FileToVoidCallback imgInputCb) {
  return Container(
    decoration: BoxDecoration(boxShadow: [
      BoxShadow(
        color: Colors.blueGrey,
        blurRadius: 0.5,
      )
    ], color: Colors.white),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Material(
          color: Colors.white,
          child: IconButton(
              highlightColor: Colors.transparent,
              icon: Icon(
                Icons.mic,
                color: Colors.blueGrey,
              ),
              onPressed: () => {debugPrint("Press micro icon button")}),
        ),
        const SizedBox(
          width: 10.0,
        ),
        Flexible(
            child: Container(
          child: TextField(
              focusNode: fn,
              controller: ctrl,
              decoration: InputDecoration.collapsed(
                hintText: "Type something...",
                //hintStyle: Styles.footnote_Grey_Left
              ),
              style: const TextStyle(fontSize: 18.0),
              cursorColor: Colors.blueGrey),
        )),
        Material(
          color: Colors.white,
          child: IconButton(
              highlightColor: Colors.transparent,
              icon: Icon(
                Icons.add,
                color: const Color(0xffee8777),
              ),
              onPressed: () async {
                var image =
                    await ImagePicker.pickImage(source: ImageSource.gallery);
                imgInputCb(image);
              }),
        ),
        Material(
          color: Colors.white,
          child: IconButton(
              highlightColor: Colors.transparent,
              icon: Icon(
                Icons.send,
                color: const Color(0xffee8777),
              ),
              onPressed: () {
                String txtInput = ctrl.text;
                txtInputCb(txtInput); // callback to make message
                // clear input
                ctrl.text = "";
                // dismiss keyboard
                fn.unfocus();
              }),
        )
      ],
    ),
  );
}

Widget buildListMessage(List<Message> messages) {
  return ListView.builder(
    padding: const EdgeInsets.only(left: 10.0, top: 20.0, right: 10.0),
    itemBuilder: (BuildContext ctx, int idx) {
      return Container(
        margin: const EdgeInsets.only(bottom: 10.0), // Margin between items
        child: messages[messages.length - idx - 1]
            .buildMessageItem(), // Prevent reverse from bottom to top
      );
    },
    itemCount: messages.length,
    reverse: true,
  );
}

enum MESSAGE_TYPE { MESSAGE_RECEIVER, MESSAGE_SENDER }
enum MESSAGE_STATUS { NONE, INFO, SEEN, SEEN_ALL }

abstract class Message {
  String lastSeenDate;

  MESSAGE_TYPE msgType;
  MESSAGE_STATUS msgStt;

  Message(this.lastSeenDate, this.msgType, this.msgStt);

  Widget buildMessageItem();

  Widget getIconStatus() {
    Icon icon;
    switch (this.msgStt) {
      case MESSAGE_STATUS.INFO:
        icon = Icon(
          Icons.info,
          size: 16.0,
          color: Colors.blue,
        );
        break;
      case MESSAGE_STATUS.SEEN:
        icon = Icon(
          Icons.done,
          size: 16.0,
          color: const Color(0xffee8777),
        );
        break;
      case MESSAGE_STATUS.SEEN_ALL:
        icon = Icon(
          Icons.done_all,
          size: 16.0,
          color: const Color(0xffee8777),
        );
        break;
      default:
        icon = null;
    }

    return icon;
  }
}

class TextMessage extends Message {
  String msgContent;
  String sender;

  TextMessage(String msgContent, String lastSeenDate,
      {MESSAGE_TYPE msgType = MESSAGE_TYPE.MESSAGE_RECEIVER,
      MESSAGE_STATUS msgStatus = MESSAGE_STATUS.NONE,
      String sender = ""})
      : super(lastSeenDate, msgType, msgStatus) {
    this.msgContent = msgContent;
    this.sender = sender;
  }

  @override
  Widget buildMessageItem() {
    return Row(
      mainAxisAlignment: this.msgType == MESSAGE_TYPE.MESSAGE_SENDER
          ? MainAxisAlignment.end
          : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TextMessageContent(
          this.sender,
          this.msgContent,
          arrowType: this.msgType == MESSAGE_TYPE.MESSAGE_SENDER
              ? ARROW_TYPE.RIGHT_ARROW
              : ARROW_TYPE.LEFT_ARROW,
          bgColor: this.msgType == MESSAGE_TYPE.MESSAGE_SENDER
              ? const Color(0xffee8777)
              : const Color(0xFFEEEEEE),
          msgSttBar: MessageStatusBar(
            this.lastSeenDate,
            icon: this.getIconStatus(),
            statusIconType: STATUS_ICON_TYPE.SUFFIX_ICON,
          ),
          titleMessageStyle: this.msgType == MESSAGE_TYPE.MESSAGE_SENDER
              ? TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: "SFProText")
              : TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: "SFProText"),
        )
      ],
    );
  }
}

enum IMAGE_LOCATION { ASSETS, GALLERY }

class ImageMessage extends Message {
  IMAGE_LOCATION imgLocation;
  String imageUrl;
  String sender;

  ImageMessage(String imageUrl, String lastSeenDate,
      {MESSAGE_TYPE msgType = MESSAGE_TYPE.MESSAGE_RECEIVER,
      MESSAGE_STATUS msgStatus = MESSAGE_STATUS.NONE,
      this.imgLocation = IMAGE_LOCATION.ASSETS,
      this.sender = ""})
      : super(lastSeenDate, msgType, msgStatus) {
    this.imageUrl = imageUrl;
    this.sender = sender;
  }

  @override
  Widget buildMessageItem() {
    return Row(
      mainAxisAlignment: this.msgType == MESSAGE_TYPE.MESSAGE_SENDER
          ? MainAxisAlignment.end
          : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        MediaMessageContent(
          300,
          200,
          this.sender,
          this.imageUrl,
          titleMessageStyle: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              fontFamily: "SFProText"),
          arrowType: this.msgType == MESSAGE_TYPE.MESSAGE_SENDER
              ? ARROW_TYPE.RIGHT_ARROW
              : ARROW_TYPE.LEFT_ARROW,
          msgSttBar: MessageStatusBar(
            this.lastSeenDate,
            icon: this.getIconStatus(),
            statusIconType: STATUS_ICON_TYPE.SUFFIX_ICON,
          ),
          mediaMessageType: MEDIA_MESSAGE_TYPE.IMAGE,
          location: this.imgLocation == IMAGE_LOCATION.ASSETS ? 0 : 1,
        )
      ],
    );
  }
}

String _getCurrentHour() {
  DateTime now = DateTime.now();
  int hour = now.hour;
  int minute = now.minute;
  String suffix = "am";
  if (hour > 12) {
    hour = hour - 12;
    suffix = "pm";
  }
  return "${hour < 10 ? "0" + hour.toString() : hour.toString()}:${minute < 10 ? "0" + minute.toString() : minute.toString()}$suffix";
}
