import 'package:cached_network_image/cached_network_image.dart';
import 'package:dacs/api/db_api.dart';
import 'package:dacs/data/images.dart';
import 'package:dacs/data/local/shared_pref_until.dart';
import 'package:dacs/models/userprofile.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  SharePrefUtil share = new SharePrefUtil();
  UserProfile user = new UserProfile();
  @override
  Widget build(BuildContext) {
    return Material(
      child: SingleChildScrollView(
        child: FutureBuilder(
          future: share.getUserProfle(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              //print(snapshot.data);
              user = snapshot.data;
              return Stack(
                children: <Widget>[
                  Container(
                    height: 600,
                    color: Colors.white,
                  ),
                  Container(
                    height: 180,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                      stops: [0.2, 1],
                      colors: [
                        // Color.fromRGBO(248, 188, 175, 1.0),
                        //Color.fromRGBO(238, 135, 119, 1.0)
                        Color.fromRGBO(15, 115, 238, 1.0),
                        Color.fromRGBO(198, 68, 252, 1.0),
                      ],
                    )),
                    child: Align(
                      //heightFactor: 100,
                      alignment: Alignment.topRight,
                      child: Container(
                        margin: const EdgeInsets.only(top: 38.0),
                        child: IconButton(
                          onPressed: () {
                            share.saveProfile(
                                'token', -1, 'name', 'phone', 'avatar');
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                '/splash', (Route<dynamic> route) => false);
                          },
                          icon: Icon(
                            Icons.assignment_return,
                            color: Colors.white,
                          ),
                          // color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 100,
                    left: 30,
                    child: InkWell(
                      onTap: () {
                        print('ontap');
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.white,
                              width: 3,
                            ),
                            borderRadius: BorderRadius.circular(10)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: CachedNetworkImage(
                            width: 90,
                            height: 130,
                            imageUrl:
                                'https://scontent.fsgn1-1.fna.fbcdn.net/v/t1.0-9/29594742_2004937379744726_1591708678947648299_n.jpg?_nc_cat=102&_nc_oc=AQk3BiBT6GxWg2aDfOzDVC_WqlGuA8EbgVZ8BFcXM5Pmkl0QxA_oB8_vuTeQ_6Y7h-k&_nc_ht=scontent.fsgn1-1.fna&oh=9b917c0e22a8b312a8d974d0a6f291ba&oe=5D9C7CDA',
                            placeholder: (context, url) => new Center(
                                  child: CircularProgressIndicator(),
                                ),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 110,
                    left: 150,
                    height: 60,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          user.name,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold),
                        ),
                        Text("Quận 3, TP HỒ CHÍ MINH",
                            style: TextStyle(color: Colors.white, fontSize: 14))
                      ],
                    ),
                  ),
                  Positioned(
                    top: 190,
                    left: 150,
                    height: 50,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          "5",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        Text("Phòng cho thuê",
                            style: TextStyle(color: Colors.grey, fontSize: 12))
                      ],
                    ),
                  ),
                  Positioned(
                    top: 190,
                    left: 270,
                    height: 50,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          "380",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        Text("Lượt xem",
                            style: TextStyle(color: Colors.grey, fontSize: 12))
                      ],
                    ),
                  ),
                  Positioned(
                      top: 250,
                      left: 30,
                      right: 30,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            "Thông tin cá nhân",
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Xem tất cả",
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                          ),
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 290, left: 30, right: 30),
                    child: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text('Số điện thoại',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                              )),
                        ),
                        TextField(
                          onChanged: (text) {},
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: '0775654423',
                              hintStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Divider(
                          height: 10.0,
                          color: Colors.black,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          alignment: Alignment.centerLeft,
                          child: Text('Tài khoản',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                              )),
                        ),
                        TextField(
                          enabled: false,
                          onChanged: (text) {},
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'phamquochuy24082013@gmail.com',
                              hintStyle:
                                  TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Divider(
                          height: 10.0,
                          color: Colors.black,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          alignment: Alignment.centerRight,
                          child: Text('Đổi mật khẩu',
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 16,
                              )),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                      top: 500,
                      left: 30,
                      right: 30,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            "Danh sách phòng đăng tải",
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Xem tất cả",
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                          ),
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.only(top: 550, left: 30),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Wrap(
                        children: List.generate(20, (index) => Story()),
                      ),
                    ),
                  )
                ],
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}

class Story extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10, bottom: 50),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: Image.asset(
          Images.imgBg,
          width: 250,
          height: 170,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
