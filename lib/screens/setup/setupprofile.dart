import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/textfield.dart';
import 'package:dacs/styles/styles.dart';
import 'package:flutter/material.dart';

class SetupProfile extends StatefulWidget {
  @override
  _SetupProfileState createState() => _SetupProfileState();
}

class _SetupProfileState extends State<SetupProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
        elevation: 0.0,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
        ),
      ),
      body: Container(
        color: Color.fromRGBO(255, 255, 255, 1.0),
        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Cập Nhật Tài Khoản',
                  // textAlign: TextAlign.center,
                  style: StylesText.style24BlackMontserratBold,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              CustomTextField(
                label: 'Full Name',
              ),
              SizedBox(
                height: 30,
              ),
              Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text('Giới Tính'),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    //height: 80,
                    padding: EdgeInsets.only(left: 3, right: 3),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child: CusTomOutlineButtom(
                              //height: 40,
                              onPressed: () {},
                              title: 'Nam',
                              borderColor: Colors.blue,
                              fillColor: Colors.white,
                              borderRadius: 2.0,
                              textColor: Colors.blue,
                            )),
                        Expanded(
                            flex: 1,
                            child: CusTomOutlineButtom(
                              height: 50,
                              onPressed: () {},
                              title: 'Nữ',
                              // borderColor: Colors.white,
                              fillColor: Colors.blue,
                              borderRadius: 0.0,
                              textColor: Colors.white,
                            )),
                        Expanded(
                            flex: 1,
                            child: CusTomOutlineButtom(
                              // height: 40,
                              onPressed: () {},
                              title: 'Khác',
                              borderColor: Colors.blue,
                              fillColor: Colors.white,
                              borderRadius: 2.0,
                              textColor: Colors.blue,
                            )),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                label: 'Năm sinh',
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                label: 'Email',
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                label: 'Mật Khẩu',
              ),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                label: 'Xác Nhận Mật Khẩu',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
