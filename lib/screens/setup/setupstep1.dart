import 'package:dacs/components/ui/itemlooking.dart';
import 'package:dacs/data/images.dart';
import 'package:dacs/styles/styles.dart';
import 'package:flutter/material.dart';

class Setup1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
        elevation: 0.0,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
        ),
      ),
      body: Container(
        color: Color.fromRGBO(255, 255, 255, 1.0),
        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
        constraints: BoxConstraints.expand(),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                'Bạn muốn tìm kiếm\nchủ đề nào?',
               // textAlign: TextAlign.center,
                style: StylesText.style24BlackMontserratBold,
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                ItemLooking(
                  startColor: Color.fromRGBO(88, 86, 214, 1.0),
                  endColor: Color.fromRGBO(198, 68, 252, 1.0),
                  image: Images.imgRooms,
                  title: 'Phòng',
                ),
                ItemLooking(
                  startColor: Color.fromRGBO(216, 255, 157, 1.0),
                  endColor: Color.fromRGBO(30, 66, 34, 1.0),
                  image: Images.imgPlace,
                  title: 'Nơi ở',
                ),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                ItemLooking(
                  startColor: Color.fromRGBO(0, 18, 255, 1.0),
                  endColor: Color.fromRGBO(0, 255, 128, 1.0),
                  image: Images.imgRoomates,
                  title: 'Ở ghép',
                ),
                ItemLooking(
                  startColor: Color.fromRGBO(255, 94, 58, 1.0),
                  endColor: Color.fromRGBO(255, 149, 0, 1.0),
                  image: Images.imgTenant,
                  title: 'Người thuê nhà',
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
