import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dacs/api/db_api.dart';
import 'package:dacs/blocs/search/searchbloc.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_location.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_upload.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/starrating.dart';
import 'package:dacs/models/district.dart';
import 'package:dacs/models/province.dart';
import 'package:dacs/models/room.dart';
import 'package:dacs/screens/home/DetailRoom.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

String baseURl = Url.baseURL;
var formatter = NumberFormat("###,###,###.##", "vi");
var intl = NumberFormat.compact();

class FindScreen extends StatefulWidget {
  @override
  _FindScreenState createState() => _FindScreenState();
}

class _FindScreenState extends State<FindScreen> {
  TextEditingController controller = TextEditingController();

  SearchBloc searchBloc;
  @override
  void initState() {
    super.initState();
    searchBloc = new SearchBloc([], [], '', 1000000.0, 10000000.0);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SearchBloc>(
      builder: (context) => searchBloc,
      child: ChildSearchScreen(),
    );
  }
}

class ChildSearchScreen extends StatefulWidget {
  @override
  _ChildSearchScreenState createState() => _ChildSearchScreenState();
}

class _ChildSearchScreenState extends State<ChildSearchScreen> {
  TextEditingController controller = TextEditingController();

  List<Widget> lstSearchScreen;
  District _district = new District();
  @override
  void initState() {
    lstSearchScreen = [
      FirstSearch(),
    ];
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final searchBloc = Provider.of<SearchBloc>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        elevation: 0.0,
        title: Text(
          'Tìm kiếm',
        ),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Container(
            color: Colors.blue,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Row(
                  children: <Widget>[
                    MaterialButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            child: FutureBuilder<List<District>>(
                              future: api.fetchDistrictById('1'),
                              builder: (BuildContext context,
                                  AsyncSnapshot<List<District>> snapshot) {
                                if (snapshot.hasData) {
                                  return AlertDialog(
                                    content: Container(
                                      width: 100,
                                      height: 200,
                                      child: ListView.builder(
                                        itemCount: snapshot.data.length,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            margin:
                                                const EdgeInsets.only(top: 8.0),
                                            child: CheckButton(
                                              onPressed: () {
                                                setState(() {
                                                  _district.id =
                                                      snapshot.data[index].id;
                                                  _district.name =
                                                      snapshot.data[index].name;
                                                });
                                                // fetalldanhsasch theo quận
                                                searchBloc.fetchsearch(
                                                    _district.name);
                                                Navigator.pop(context);
                                              },
                                              title: snapshot.data[index].name,
                                              width: 70,
                                              color: Colors.grey,
                                              height: 40,
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  );
                                } else {
                                  return AlertDialog(
                                    content: Container(
                                      width: 100,
                                      height: 200,
                                      child: Center(
                                          child: CircularProgressIndicator()),
                                    ),
                                  );
                                }
                              },
                            ));
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.location_city,
                            color: Colors.pink,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            _district.name ?? 'Quận 3',
                            style: TextStyle(color: Colors.pink),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        controller: controller,
                        decoration: InputDecoration(
                            hintText: 'Tìm kiếm', border: InputBorder.none),
                        onChanged: searchBloc.onSearchTextChanged,
                        onSubmitted: (text) {
                          //  searchBloc.fetchListSuggestions(text);
                        },
                        onTap: () {
                          //searchBloc.setIndex(0);
                        },
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          controller.clear();

                          searchBloc.resetSearchResult();
                          searchBloc.setTextSearch('');
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            color: Colors.blue,
            child: FlutterSlider(
              min: 500000,
              max: 15000000.0,
              jump: true,
              values: [
                searchBloc.getLowerValueSlider(),
                searchBloc.getUpperSlider()
              ],
              step: 500000,
              rangeSlider: true,
              onDragging: searchBloc.onSearchSliderchanged,
              // visibleTouchArea: true,
              handlerAnimation: FlutterSliderHandlerAnimation(
                  curve: Curves.elasticOut,
                  reverseCurve: Curves.bounceIn,
                  duration: Duration(milliseconds: 500),
                  scale: 1.5),
              handler: FlutterSliderHandler(
                decoration: BoxDecoration(),
                child: Container(
                  width: 15,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              rightHandler: FlutterSliderHandler(
                decoration: BoxDecoration(),
                child: Container(
                  width: 15,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              trackBar: FlutterSliderTrackBar(
                activeTrackBarColor: Colors.white,
                activeTrackBarHeight: 3.5,
                inactiveTrackBarColor: Colors.white,
              ),
              disabled: false,
              tooltip: FlutterSliderTooltip(
                alwaysShowTooltip: true,
                numberFormat: intl,
                textStyle: TextStyle(
                  color: Colors.white,
                ),
                boxStyle: FlutterSliderTooltipBox(
                  decoration: BoxDecoration(color: Colors.transparent),
                ),
              ),
            ),
          ),
          FirstSearch()
        ],
      ),
    );
  }
}

class FirstSearch extends StatefulWidget {
  @override
  _FirstSearchState createState() => _FirstSearchState();
}

class _FirstSearchState extends State<FirstSearch> {
  @override
  Widget build(BuildContext context) {
    final searchBloc = Provider.of<SearchBloc>(context);

    return Expanded(
      child: searchBloc.getSearchResult().length == 0
          ? Text('')
          : searchBloc.getroomDetails().length != 0
              ? Column(
                  children: <Widget>[
                    Text(
                        'Có ${searchBloc.getroomDetails().length} kết quả tìm kiếm'),
                    Expanded(
                      child: ListView.builder(
                          itemCount: searchBloc.getroomDetails().length,
                          itemBuilder: (context, index) {
                            return Column(
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => RoomDetail(
                                                room: searchBloc
                                                    .getroomDetails()[index],
                                              ),
                                        ));
                                  },
                                  child: Card(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            width: 150,
                                            height: 100,
                                            child: CachedNetworkImage(
                                              imageUrl:
                                                  '$baseURl/image/${searchBloc.getroomDetails()[index].image[0]}',
                                              placeholder: (context, url) =>
                                                  Center(
                                                    child:
                                                        CircularProgressIndicator(),
                                                  ),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Icon(Icons.error),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          Expanded(
                                            child: Column(
                                              children: <Widget>[
                                                Text(
                                                  searchBloc
                                                      .getroomDetails()[index]
                                                      .name,
                                                  style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        2, 4, 51, 1.0),
                                                    fontFamily:
                                                        'Montserrat-Bold',
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  formatter
                                                          .format(searchBloc
                                                              .getroomDetails()[
                                                                  index]
                                                              .price)
                                                          .toString() +
                                                      ' VND / tháng',
                                                  style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        166, 29, 85, 1.0),
                                                    fontFamily:
                                                        'Montserrat-Bold',
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                                StarRating(
                                                  color: Colors.amber,
                                                  rating: 4,
                                                  starCount: 5,
                                                  size: 20,
                                                ),
                                                Text(
                                                  'Địa chỉ: ' +
                                                      '${searchBloc.getroomDetails()[index].address} ' +
                                                      '${searchBloc.getroomDetails()[index].ward} ' +
                                                      '${searchBloc.getroomDetails()[index].district} ',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        2, 4, 51, 1.0),
                                                    fontFamily:
                                                        'Montserrat-Bold',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    margin: const EdgeInsets.all(0.0),
                                  ),
                                ),
                                Divider(),
                              ],
                            );
                          }),
                    ),
                  ],
                )
              : ListView.builder(
                  itemCount: searchBloc.getSearchResult().length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => RoomDetail(
                                        room:
                                            searchBloc.getSearchResult()[index],
                                      ),
                                ));
                          },
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: 150,
                                    height: 100,
                                    child: CachedNetworkImage(
                                      imageUrl:
                                          '$baseURl/image/${searchBloc.getSearchResult()[index].image[0]}',
                                      placeholder: (context, url) => Center(
                                            child: CircularProgressIndicator(),
                                          ),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          searchBloc
                                              .getSearchResult()[index]
                                              .name,
                                          style: TextStyle(
                                            color:
                                                Color.fromRGBO(2, 4, 51, 1.0),
                                            fontFamily: 'Montserrat-Bold',
                                            fontSize: 15,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          formatter
                                                  .format(searchBloc
                                                      .getSearchResult()[index]
                                                      .price)
                                                  .toString() +
                                              ' VND / tháng',
                                          style: TextStyle(
                                            color: Color.fromRGBO(
                                                166, 29, 85, 1.0),
                                            fontFamily: 'Montserrat-Bold',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        StarRating(
                                          color: Colors.amber,
                                          rating: 4,
                                          starCount: 5,
                                          size: 20,
                                        ),
                                        Text(
                                          'Địa chỉ: ' +
                                              '${searchBloc.getSearchResult()[index].address} ' +
                                              '${searchBloc.getSearchResult()[index].ward} ' +
                                              '${searchBloc.getSearchResult()[index].district} ',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color:
                                                Color.fromRGBO(2, 4, 51, 1.0),
                                            fontFamily: 'Montserrat-Bold',
                                            fontSize: 13,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            margin: const EdgeInsets.all(0.0),
                          ),
                        ),
                        Divider(),
                      ],
                    );
                  }),
    );
  }
}
