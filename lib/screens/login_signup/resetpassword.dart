import 'package:dacs/blocs/user_bloc.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/textfield.dart';
import 'package:dacs/screens/login_signup/login.dart';
import 'package:dacs/screens/login_signup/success.dart';
import 'package:dacs/styles/styles.dart';

import 'package:flutter/material.dart';

class ResetPassWord extends StatefulWidget {
  @override
  _ResetPassWordState createState() => _ResetPassWordState();
}

class _ResetPassWordState extends State<ResetPassWord> {
  UpdatePasswordBloc _bloc;
  bool isClick = false;
  bool isClick1 = false; // confirm
  @override
  void initState() {
    super.initState();
    _bloc = new UpdatePasswordBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
        elevation: 0.0,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
        ),
      ),
      body: Container(
        color: Color.fromRGBO(255, 255, 255, 1.0),
        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Cập Nhật Mật Khẩu',
                    style: StylesText.style24BlackMontserratBold,
                  )),
              SizedBox(
                height: 20,
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Mã đã được gửi, Vui lòng nhập mã và tạo mật khẩu mới ',
                    style: StylesText.style16GrayMontserratRegular,
                  )),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _bloc.token,
                builder: (context, snapshot) {
                  return CustomTextField(
                    label: 'Mã Xác Nhận',
                    prefixIcon: Icon(Icons.zoom_out_map),
                    suffixIcon: null,
                    errorText: snapshot.error,
                    onChange: _bloc.onTokenChanged,
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _bloc.password,
                builder: (context, snapshot) {
                  return TextField(
                    decoration: InputDecoration(
                      errorText: snapshot.error,
                      prefixIcon: Icon(Icons.vpn_key,
                          color: snapshot.hasError ? Colors.red : Colors.blue),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            isClick = !isClick;
                          });
                        },
                        icon: Icon(Icons.remove_red_eye),
                        color: isClick == true ? Colors.blue : Colors.grey,
                      ),
                      enabledBorder: const OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      labelText: 'Mật khẩu',
                    ),
                    onChanged: _bloc.onPasswordChanged,
                    obscureText: !isClick,
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _bloc.confirmpassword,
                builder: (context, snapshot) {
                  return TextField(
                    decoration: InputDecoration(
                      errorText: snapshot.error,
                      prefixIcon: Icon(Icons.vpn_key,
                          color: snapshot.hasError ? Colors.red : Colors.blue),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            isClick1 = !isClick1;
                          });
                        },
                        icon: Icon(Icons.remove_red_eye),
                        color: isClick1 == true ? Colors.blue : Colors.grey,
                      ),
                      enabledBorder: const OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      labelText: 'Xác nhận Mật khẩu',
                    ),
                    onChanged: _bloc.onConfirmPasswordChanged,
                    obscureText: !isClick1,
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder<bool>(
                stream: _bloc.submit,
                builder: (context, snapshot) {
                  return CommonButton(
                    startColor: Color.fromRGBO(15, 115, 238, 1.0), // xanh
                    endColor: Color.fromRGBO(198, 68, 252, 1.0), // tím
                    title: 'THAY ĐỔI MẬT KHẨU',
                    onPressed: () async {
                      if (snapshot.hasData && snapshot.data == true) {
                        await _bloc.onSubmit().then((val) {
                          // xử lí sai email pass if val = 404
                          // print(val);
                          val == 404
                              ? Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text(
                                      'Thông tin xác thực không dúng không đúng',
                                      textAlign: TextAlign.center),
                                  duration: Duration(seconds: 3),
                                ))
                              : Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Success(
                                          onPressed: () {
                                            Navigator.of(context)
                                                .pushNamedAndRemoveUntil(
                                                    '/login',
                                                    (Route<dynamic> route) =>
                                                        false);
                                          },
                                          subtitle:
                                              'Bạn đã thay đổi mật khẩu thành công. Vui lòng sử dụng mật khẩu mới của bạn khi đăng nhập',
                                          titleButton: 'ĐĂNG NHẬP NGAY',
                                        ),
                                  ),
                                );
                          //  Navigator.pushNamed(context, "/succcess");
                        });
                      }
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
