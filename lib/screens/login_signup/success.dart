import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/data/images.dart';
import 'package:dacs/screens/login_signup/login.dart';
import 'package:dacs/styles/styles.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';

class Success extends StatelessWidget {
  final String subtitle;
  final String titleButton;
  final VoidCallback onPressed;

  Success({this.onPressed, this.subtitle, this.titleButton});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Dimension.getWidth(1.0),
        height: Dimension.getHeight(1.0),
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Color.fromRGBO(15, 115, 238, 1.0), // xanh
            Color.fromRGBO(198, 68, 252, 1.0), // tim
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: Dimension.getHeight(0.2),
              width: Dimension.getWidth(0.7),
              child: Image.asset(
                Images.imgSuccess,
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              child: Text(
                'Thành công',
                style: StylesText.style24WhiteMontserratBold,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                subtitle,
                // 'Bạn đã thay đổi mật khẩu thành công. Vui lòng sử dụng mật khẩu mới của bạn khi đăng nhập',
                style: StylesText.style14WhiteBalooBhai,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CheckButton(
              title: titleButton,
              width: Dimension.getWidth(0.4),
              height: 50,
              colorTitle: Colors.black,
              color: Colors.white,
              onPressed: onPressed,
            )
          ],
        ),
      ),
    );
  }
}
