import 'package:dacs/blocs/user_bloc.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/textfield.dart';
import 'package:dacs/screens/login_signup/resetpassword.dart';
import 'package:dacs/screens/login_signup/signup.dart';
import 'package:dacs/styles/styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class FoGotPassWord extends StatefulWidget {
  @override
  _FoGotPassWordState createState() => _FoGotPassWordState();
}

class _FoGotPassWordState extends State<FoGotPassWord> {
  ForgotPassWordBloc _bloc;
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    _bloc = new ForgotPassWordBloc();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
        elevation: 0.0,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
        ),
      ),
      body: Container(
        color: Color.fromRGBO(255, 255, 255, 1.0),
        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Quên Mật Khẩu?',
                    style: StylesText.style24BlackMontserratBold,
                  )),
              SizedBox(
                height: 20,
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Nhập địa chỉ email để lấy lại mật khẩu',
                    style: StylesText.style16GrayMontserratRegular,
                  )),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _bloc.email,
                builder: (context, snapshot) {
                  return CustomTextField(
                    label: 'Email',
                    prefixIcon: Icon(Icons.email,
                        color: snapshot.hasError ? Colors.red : Colors.blue),
                    suffixIcon: null,
                    errorText: snapshot.error,
                    onChange: _bloc.onEmailChanged,
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder<bool>(
                stream: _bloc.submitValid,
                builder: (context, snapshot) {
                  return CommonButton(
                    title: 'GỬI YÊU CẦU',
                    startColor: Color.fromRGBO(15, 115, 238, 1.0), // xanh
                    endColor: Color.fromRGBO(198, 68, 252, 1.0), // tím
                    onPressed: () async {
                      //print('hello');
                      setState(() {
                        isLoading = false;
                      });

                      Future.delayed(const Duration(seconds: 3), () {
                        setState(() {
                          isLoading = true;
                        });
                      });
                      if (snapshot.hasData == true || snapshot.data == true) {
                        await _bloc.onSubmit().then((val) {
                          print(val);
                          val == 404
                              ? Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text('Email không tồn lại'),
                                ))
                              : Navigator.pushNamed(context, "/reset-password");
                        });
                      }
                    },
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              Opacity(
                opacity: isLoading == true ? 0.0 : 1.0,
                child: CircularProgressIndicator(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
