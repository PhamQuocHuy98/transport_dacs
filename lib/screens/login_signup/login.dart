import 'package:dacs/blocs/user_bloc.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/textfield.dart';
import 'package:dacs/screens/home/Home.dart';
import 'package:dacs/screens/home/Main.dart';

import 'package:dacs/screens/login_signup/fogotpassword.dart';
import 'package:dacs/screens/login_signup/signup.dart';

import 'package:dacs/styles/styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool isClick = false;
  LoginBloc _loginBloc;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _loginBloc = LoginBloc();
  }

  @override
  void dispose() {
    _loginBloc?.dispose();
    super.dispose();
  }

  final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
        elevation: 0.0,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
        ),
      ),
      body: Container(
        color: Color.fromRGBO(255, 255, 255, 1.0),
        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Xin Chào',
                    style: StylesText.style24BlackMontserratBold,
                  )),
              SizedBox(
                height: 10,
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Bạn đã trở lại!',
                    style: StylesText.style24BlackMontserratBold,
                  )),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _loginBloc.email,
                builder: (context, snapshot) {
                  return CustomTextField(
                    label: 'Email',
                    prefixIcon: Icon(Icons.email,
                        color: snapshot.hasError ? Colors.red : Colors.blue),
                    suffixIcon: null,
                    errorText: snapshot.error,
                    onChange: _loginBloc.onEmailChanged,
                  );
                },
              ),
              SizedBox(
                height: 20,
              ),
              StreamBuilder(
                stream: _loginBloc.password,
                builder: (context, snapshot) {
                  return TextField(
                    decoration: InputDecoration(
                      errorText: snapshot.error,
                      prefixIcon: Icon(Icons.vpn_key,
                          color: snapshot.hasError ? Colors.red : Colors.blue),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            isClick = !isClick;
                          });
                        },
                        icon: Icon(Icons.remove_red_eye),
                        color: isClick == true ? Colors.blue : Colors.grey,
                      ),
                      enabledBorder: const OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      labelText: 'Mật khẩu',
                    ),
                    onChanged: _loginBloc.onPasswordChanged,
                    obscureText: !isClick,
                  );
                },
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/fogot-password");
                },
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Quên mật khẩu?',
                    style: StylesText.style16GrayMontserratRegular,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder<bool>(
                stream: _loginBloc.loginValid,
                builder: (context, snapshot) {
                  // print(snapshot.data);
                  return CommonButton(
                    title: 'Đăng Nhập',
                    onPressed: () async {
                      setState(() {
                        isLoading = true;
                      });
                      Future.delayed(const Duration(seconds: 1), () {
                        setState(() {
                          isLoading = false;
                        });
                      });
                      if (snapshot.hasData && snapshot.data == true) {
                        await _loginBloc.submit(context).then((val) {
                          // xử lí sai email pass if val = 404
                          val == 404
                              ? Scaffold.of(context).showSnackBar(SnackBar(
                                  content: Text(
                                      'Tài khoản hoặc mật khẩu không đúng',
                                      textAlign: TextAlign.center),
                                  duration: Duration(seconds: 3),
                                ))
                              : Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MainScreen(),
                                  ));
                        });
                      }
                    },
                    startColor: Color.fromRGBO(15, 115, 238, 1.0), // xanh
                    endColor: Color.fromRGBO(198, 68, 252, 1.0), // tím
                  );
                },
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                children: <Widget>[
                  _buildButtonLogoLogin('Facebook', Color(0xFF4364A1)),
                  SizedBox(
                    width: 20,
                  ),
                  _buildButtonLogoLogin('Google', Color(0xFFDF513B)),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Opacity(
                opacity: isLoading == true ? 1.0 : 0.0,
                child: CircularProgressIndicator(),
              ),
              SizedBox(
                height: 24.0,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 40),
                child: RichText(
                  text: TextSpan(
                      text: 'Bạn chưa có tài khoản?',
                      style: TextStyle(color: Color(0xff606470), fontSize: 16),
                      children: <TextSpan>[
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SignUp()));
                              },
                            text: 'Đăng ký ',
                            style: TextStyle(
                              color: Color(0xff3277D8),
                            ))
                      ]),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buildButtonLogoLogin(String text, Color color) {
  return Expanded(
    child: InkWell(
      onTap: () {},
      child: Container(
        alignment: Alignment.center,
        height: 60.0,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(text, style: StylesText.style14WhiteBalooBhai),
      ),
    ),
  );
}
