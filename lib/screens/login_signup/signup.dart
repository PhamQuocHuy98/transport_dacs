import 'package:dacs/blocs/user_bloc.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/textfield.dart';
import 'package:dacs/styles/styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool isClick = false;
  RegisterBloc _registerBloc;
  @override
  void initState() {
    super.initState();
    _registerBloc = new RegisterBloc();
  }

  @override
  void dispose() {
    _registerBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //  final UserBloc userBloc = BlocProvider.of<UserBloc>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
        elevation: 0.0,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
        ),
      ),
      body: Container(
        color: Color.fromRGBO(255, 255, 255, 1.0),
        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Xin Chào,',
                    style: StylesText.style24BlackMontserratBold,
                  )),
              SizedBox(
                height: 10,
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Bạn đến với chúng tôi!',
                    style: StylesText.style24BlackMontserratBold,
                  )),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _registerBloc.name,
                builder: (context, snapshot) {
                  return CustomTextField(
                    label: 'Họ tên ',
                    errorText: snapshot.error,
                    prefixIcon: Icon(Icons.person,
                        color: snapshot.hasError ? Colors.red : Colors.blue),
                    suffixIcon: null,
                    onChange: _registerBloc.onNameChanged,
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _registerBloc.email,
                builder: (context, snapshot) {
                  return CustomTextField(
                    label: 'Email ',
                    errorText: snapshot.error,
                    prefixIcon: Icon(Icons.email,
                        color: snapshot.hasError ? Colors.red : Colors.blue),
                    suffixIcon: null,
                    onChange: _registerBloc.onEmailChanged,
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _registerBloc.phone,
                builder: (context, snapshot) {
                  return CustomTextField(
                    // controller: phone,
                    label: 'Số điện thoại ',
                    prefixIcon: Icon(Icons.phone,
                        color: snapshot.hasError ? Colors.red : Colors.blue),
                    suffixIcon: null,
                    errorText: snapshot.error,
                    onChange: _registerBloc.onPhoneChanged,
                  );
                },
              ),
              SizedBox(
                height: 20,
              ),
              StreamBuilder(
                stream: _registerBloc.password,
                builder: (context, snapshot) {
                  return TextField(
                    decoration: InputDecoration(
                      errorText: snapshot.error,
                      prefixIcon: Icon(Icons.vpn_key,
                          color: snapshot.hasError ? Colors.red : Colors.blue),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            isClick = !isClick;
                          });
                        },
                        icon: Icon(Icons.remove_red_eye),
                        color: isClick == true ? Colors.blue : Colors.grey,
                      ),
                      enabledBorder: const OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      labelText: 'Mật khẩu',
                    ),
                    onChanged: _registerBloc.onPasswordChanged,
                    obscureText: !isClick,
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              StreamBuilder(
                stream: _registerBloc.registerValid,
                builder: (context, snapshot) {
                  return CommonButton(
                    startColor: Color.fromRGBO(15, 115, 238, 1.0), // xanh
                    endColor: Color.fromRGBO(198, 68, 252, 1.0), // tím
                    title: 'Đăng Ký',
                    onPressed: () async {
                      await _registerBloc.onSubmit().then((res) {
                        res == 201
                            ? Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text('Đăng kí thành công'),
                              ))
                            : Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text('Đăng kí thất bại'),
                              ));
                      });
                    },
                  );
                },
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 40),
                child: RichText(
                  text: TextSpan(
                      text: 'Đồng ý ?',
                      style: TextStyle(color: Color(0xff606470), fontSize: 16),
                      children: <TextSpan>[
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.pop(context);
                              },
                            text: 'Đăng Nhập ',
                            style: TextStyle(
                              color: Color(0xff3277D8),
                            ))
                      ]),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
