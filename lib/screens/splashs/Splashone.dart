import 'package:dacs/components/ui/slider.dart';
import 'package:dacs/styles/styles.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';

class Splash extends StatefulWidget {
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Dimension.height = MediaQuery.of(context).size.height;
    Dimension.witdh = MediaQuery.of(context).size.width;
    SizeText.queryData = MediaQuery.of(context).textScaleFactor;
    return Scaffold(body: IntroSlider());
  }
}
