import 'dart:io';

import 'package:dacs/blocs/home/homebloc.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_done.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_location.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/data/local/shared_pref_until.dart';
import 'package:dacs/models/room.dart';
import 'package:dacs/routes/route.dart';
import 'package:dacs/screens/home/DetailRoom.dart';
import 'package:dacs/screens/home/Home.dart';
import 'package:dacs/screens/uploadroom/uploadroom.dart';
import 'package:dacs/screens/user/profile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MainScreen();
  }
}

class _MainScreen extends State<MainScreen> {
  final homeBloc = new HomeBloc();

  // final profileBloc  =
  int _currentIndex = 0;
  final List<Widget> _children = [
    Home(),
    Home(),
    Home(),
    Profile(),
  ];

  @override
  void dispose() {
  //  homeBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<HomeBloc>(
          builder: (_) => homeBloc,
        ),
      ],
      child: WillPopScope(
        onWillPop: () => _exitApp(context),
        child: Scaffold(
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Color.fromRGBO(198, 68, 252, 1.0),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ScreenUploadRoom()),
              );
            },
            tooltip: 'Đăng tải phòng',
            child: Icon(Icons.add),
            elevation: 2.0,
          ),
          body: _children[_currentIndex],
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentIndex,
            onTap: (i) {
              setState(() {
                _currentIndex = i;
              });
            },
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  size: 20,
                  color: _currentIndex == 0
                      ? Color.fromRGBO(198, 68, 252, 1.0)
                      : Colors.grey,
                ),
                title: Text(
                  'Trang chủ',
                  style: TextStyle(
                    color: _currentIndex == 0
                        ? Color.fromRGBO(198, 68, 252, 1.0)
                        : Colors.grey,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.search,
                    size: 20,
                    color: _currentIndex == 1
                        ? Color.fromRGBO(198, 68, 252, 1.0)
                        : Colors.grey,
                  ),
                  title: Text('Ở ghép',
                      style: TextStyle(
                        color: _currentIndex == 1
                            ? Color.fromRGBO(198, 68, 252, 1.0)
                            : Colors.grey,
                      ))),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.event_note,
                    size: 20,
                    color: _currentIndex == 2
                        ? Color.fromRGBO(198, 68, 252, 1.0)
                        : Colors.grey,
                  ),
                  title: Text('Ưu đãi',
                      style: TextStyle(
                        color: _currentIndex == 2
                            ? Color.fromRGBO(198, 68, 252, 1.0)
                            : Colors.grey,
                      ))),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.perm_contact_calendar,
                    size: 20,
                    color: _currentIndex == 3
                        ? Color.fromRGBO(198, 68, 252, 1.0)
                        : Colors.grey,
                  ),
                  title: Text('Tài khoản',
                      style: TextStyle(
                        color: _currentIndex == 3
                            ? Color.fromRGBO(198, 68, 252, 1.0)
                            : Colors.grey,
                      ))),
            ],
            type: BottomNavigationBarType.fixed,
          ),
        ),
      ),
    );
  }
}

Future<bool> _exitApp(BuildContext context) {
  return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text('Thoát khoải ứng dụng?'),
          content: new Text('Bạn chắc chắn muốn thoát khoải ứng dụng?'),
          actions: <Widget>[
            new FlatButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: new Text('Không'),
            ),
            new FlatButton(
              onPressed: () => exit(0), //=> Navigator.of(context).pop(true),
              child: new Text('Thoát'),
            ),
          ],
        ),
      ) ??
      false;
}
