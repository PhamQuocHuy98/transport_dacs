import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:dacs/blocs/home/homebloc.dart';
import 'package:dacs/components/ui/itemreviewmessage.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/starrating.dart';
import 'package:dacs/data/images.dart';
import 'package:dacs/models/room.dart';
import 'package:dacs/screens/chat/chatscreen.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

String baseURl = Url.baseURL;

var formatter = new NumberFormat("###,###,###.##", "vi");

class RoomDetail extends StatefulWidget {
  final Result room;

  RoomDetail({Key key, this.room}) : super(key: key);
  @override
  _RoomDetailState createState() => _RoomDetailState();
}

class _RoomDetailState extends State<RoomDetail> {
  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    Widget _image = new Container(
      //width: 100,
      height: 200,
      child: Carousel(
        boxFit: BoxFit.none,
        images: List<ImageProvider>.generate(widget.room.image.length, (index) {
          return NetworkImage('$baseURl/image/${widget.room.image[index]}'
              /*imageUrl: '$baseURl/image/${widget.room.image[index]}',
            placeholder: (context, url) => new Center(
                  child: CircularProgressIndicator(),
                ),
            errorWidget: (context, url, error) => new Icon(Icons.error),
            fit: BoxFit.cover,*/
              );
        }),
        autoplay: false,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        indicatorBgPadding: 2.0,
      ),
    );
    return Scaffold(
      bottomNavigationBar: Container(
        // color: Colors.grey[200],
        alignment: Alignment.center,
        height: 75,
        child: CheckButton(
          onPressed: () {
            Navigator.push(context,MaterialPageRoute(
              builder: (context)=>ChatScreen()
            ));
          },
          title: 'Liên hệ',
          color: Color.fromRGBO(198, 68, 252, 1.0),
         // width: 200,
          height: 60,
        ),
      ),
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Color.fromRGBO(198, 68, 252, 1.0),
            iconTheme: IconThemeData(color: Colors.white),
            expandedHeight: Dimension.getWidth(0.7),
            pinned: true,
            automaticallyImplyLeading: true,
            actions: <Widget>[
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.favorite_border),
              )
            ],
            centerTitle: true,
            title: Text('Phòng Trọ'),
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                height: Dimension.getWidth(0.7) + statusBarHeight,
                width: Dimension.getWidth(1.0),
                child: _image,
              ),
            ),
          ),
          // Widget body
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.only(bottom: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    // margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Text(
                      widget.room.name,
                      style: TextStyle(
                        color: Color.fromRGBO(2, 4, 51, 1.0),
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Text(
                      formatter.format(widget.room.price).toString() +
                          ' VND / tháng',
                      style: TextStyle(
                        color: Color.fromRGBO(166, 29, 85, 1.0),
                        fontFamily: 'Montserrat-Regular',
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: StarRating(
                      color: Colors.amber,
                      rating: 4,
                      starCount: 5,
                      size: 20,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 20,
                    color: Colors.grey[300],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Text(
                      'Người Đăng',
                      style: TextStyle(
                        color: Color.fromRGBO(2, 4, 51, 1.0),
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Row(
                      children: <Widget>[
                        CircleAvatar(
                          radius: 30,
                          child: Text(widget.room.username[0]),
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              padding:
                                  EdgeInsets.only(top: 10, left: 20, right: 20),
                              child: Text(
                                widget.room.username,
                                style: TextStyle(
                                  color: Color.fromRGBO(2, 4, 51, 1.0),
                                  fontFamily: 'Montserrat-Regular',
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                            Container(
                              padding:
                                  EdgeInsets.only(top: 10, left: 20, right: 20),
                              child: Text(
                                widget.room.phone,
                                style: TextStyle(
                                  color: Color.fromRGBO(2, 4, 51, 1.0),
                                  fontFamily: 'Montserrat-Regular',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        IconButton(
                          onPressed: () {
                            launch("tel:${widget.room.phone}");
                          },
                          icon: Icon(Icons.phone),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Text(
                      'Địa chỉ: ' +
                          '${widget.room.address} ' +
                          '${widget.room.ward} ' +
                          '${widget.room.district} ' +
                          '${widget.room.province}',
                      style: TextStyle(
                        color: Color.fromRGBO(2, 4, 51, 1.0),
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Text(
                      'Sức chứa: ' + '${widget.room.capacity} ',
                      style: TextStyle(
                        color: Color.fromRGBO(166, 29, 85, 1.0),
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 20,
                    color: Colors.grey[300],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Text(
                      'Mô Tả',
                      style: TextStyle(
                        color: Color.fromRGBO(2, 4, 51, 1.0),
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Text(
                      widget.room.description,
                      style: TextStyle(
                        color: Color.fromRGBO(2, 4, 51, 1.0),
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 20,
                    color: Colors.grey[300],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Text(
                      'Đánh Giá',
                      style: TextStyle(
                        color: Color.fromRGBO(2, 4, 51, 1.0),
                        fontFamily: 'Montserrat-Bold',
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Wrap(
                    children: List.generate(5, (index) => ItemReviewMessage()),
                  ),
                  Center(
                      child: Text(
                    'Xem tất cả',
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
