import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dacs/api/db_api.dart';
import 'package:dacs/blocs/home/homebloc.dart';
import 'package:dacs/components/ui/shimmerpage.dart';

import 'package:dacs/components/widgets/roomrecomended.dart';
import 'package:dacs/components/widgets/starrating.dart';
import 'package:dacs/data/images.dart';
import 'package:dacs/data/local/shared_pref_until.dart';
import 'package:dacs/models/room.dart';
import 'package:dacs/routes/route.dart';
import 'package:dacs/screens/find/findscreen.dart';
import 'package:dacs/screens/home/DetailRoom.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

String baseURl = Url.baseURL;

var formatter = new NumberFormat("###,###,###.##", "vi");

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Home();
  }
}

class _Home extends State<Home> {
  SharePrefUtil share = new SharePrefUtil();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final homeBloc = Provider.of<HomeBloc>(context);
    return Scaffold(
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          HomeScreenTopPart(),
          SliverToBoxAdapter(
            child: HomeScreenBody(
              bloc: homeBloc,
            ),
          )
        ],
      ),
    );
  }
}

const TextStyle dropDownLabelStyle =
    TextStyle(color: Colors.white, fontSize: 16.0);

const TextStyle dropDownMenuItemsStyle =
    TextStyle(color: Colors.black, fontSize: 16.0);

class HomeScreenTopPart extends StatefulWidget {
  @override
  _HomeScreenTopPartState createState() => _HomeScreenTopPartState();
}

class _HomeScreenTopPartState extends State<HomeScreenTopPart> {
  var selectedLocationIndex = 0;
  var isFlightSelected = true;

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent));
    return SliverAppBar(
      backgroundColor: Color.fromRGBO(198, 68, 252, 1.0),
      iconTheme: IconThemeData(color: Colors.white),
      expandedHeight: Dimension.getWidth(0.83),
      pinned: true,
      automaticallyImplyLeading: false,
      centerTitle: true,
      title: Text('Phòng Trọ'),
      flexibleSpace: FlexibleSpaceBar(
          background: Stack(
        children: <Widget>[
          Container(
            height: Dimension.getWidth(1.0) + statusBarHeight,
            width: Dimension.getWidth(1.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    // bottomRight: Radius.circular(100),
                    ),
                gradient: LinearGradient(colors: [
                  Color.fromRGBO(15, 115, 238, 1.0),
                  Color.fromRGBO(198, 68, 252, 1.0),
                ])),
            child: Container(
              margin: EdgeInsets.only(top: statusBarHeight + kToolbarHeight),
              width: Dimension.getWidth(1.0),
              decoration: BoxDecoration(
                  border:
                      Border(top: BorderSide(color: Colors.white, width: 1.0))),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 50.0,
                  ),
                  Text(
                    "Tìm phòng trọ phù hợp\n Bắt đầu tìm kiếm?",
                    style: TextStyle(fontSize: 24.0, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 16.0,
                        ),
                        PopupMenuButton(
                          onSelected: (index) {
                            setState(() {
                              selectedLocationIndex = index;
                            });
                          },
                          child: Row(
                            children: <Widget>[
                              Text(
                                selectedLocationIndex == 0
                                    ? 'TP HCM'
                                    : (selectedLocationIndex == 1
                                        ? 'Đà Nẵng'
                                        : 'Hà Nội'),
                                style: dropDownLabelStyle,
                              ),
                              Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.white,
                              )
                            ],
                          ),
                          itemBuilder: (BuildContext context) =>
                              <PopupMenuItem<int>>[
                                PopupMenuItem(
                                  child: Text(
                                    'TP HCM',
                                    style: dropDownMenuItemsStyle,
                                  ),
                                  value: 0,
                                ),
                                PopupMenuItem(
                                  child: Text(
                                    'Đà Nẵng',
                                    style: dropDownMenuItemsStyle,
                                  ),
                                  value: 1,
                                ),
                                PopupMenuItem(
                                  child: Text(
                                    'Hà Nội',
                                    style: dropDownMenuItemsStyle,
                                  ),
                                  value: 2,
                                )
                              ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Hero(
                    tag: 'mytag',
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 32.0),
                      child: Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        child: TextField(
                          style: dropDownMenuItemsStyle,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FindScreen()),
                            );
                          },
                          //   cursorColor: appTheme.primaryColor,
                          decoration: InputDecoration(
                            hintText: 'Nhập quận, huyện, tên đường ',
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 32.0, vertical: 14.0),
                            suffixIcon: Material(
                              elevation: 2.0,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                              child: Icon(
                                Icons.search,
                              ),
                            ),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }
}

class HomeScreenBody extends StatefulWidget {
  final HomeBloc bloc;

  HomeScreenBody({
    Key key,
    this.bloc,
  }) : super(key: key);
  @override
  _HomeScreenBodyState createState() => _HomeScreenBodyState();
}

class _HomeScreenBodyState extends State<HomeScreenBody> {
  Widget _generateListRoomRecomand(Result model) {
    return InkWell(
      onTap: () {
        print(model.roomid.toString());
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => RoomDetail(room: model)));
      },
      child: Container(
        margin: EdgeInsets.only(right: 10.0),
        color: Colors.white,
        padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
        child: Center(
          child: ImageCard(
              image: '$baseURl/image/${model.image[0]}',
              bigTitle: model.name.toString(),
              // url: Images.imgRooms,
              subTitle: formatter.format(model.price).toString() + ' VND',
              width: 240.0),
        ),
      ),
    );
  }

  Widget _buildNewRoom(Result model) {
    return InkWell(
      onTap: () {
        print(model.roomid.toString());
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetail(
                    room: model,
                  )),
        );
      },
      child: Card(
        elevation: 10.0,
        margin: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 20.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                    width: Dimension.getWidth(1.0),
                    height: 150,
                    child: ClipRRect(
                      borderRadius: new BorderRadius.circular(16.0),
                      child: CachedNetworkImage(
                        imageUrl: '$baseURl/image/${model.image[0]}',
                        placeholder: (context, url) => new Center(
                              child: CircularProgressIndicator(),
                            ),
                        errorWidget: (context, url, error) =>
                            new Icon(Icons.error),
                        fit: BoxFit.cover,
                      ),
                    )),
                Positioned(
                  bottom: 0,
                  left: 10,
                  child: MaterialButton(
                    color: Color.fromRGBO(58, 58, 66, 1.0),
                    onPressed: () {},
                    child: Text(
                      formatter.format(model.price).toString(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 20.0, top: 10),
              alignment: Alignment.centerLeft,
              child: Text(
                model.name.toString(),
                style: TextStyle(
                    fontFamily: 'FiraSans-Medium-',
                    color: Colors.black,
                    fontSize: 20),
              ),
            ),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 20.0, top: 10),
                  alignment: Alignment.centerLeft,
                  child: StarRating(
                    color: Colors.amber,
                    rating: 4,
                    starCount: 5,
                    size: 20,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 10, top: 10),
                  child: Text(
                    '98 Review',
                    style: TextStyle(
                      fontFamily: 'FiraSans-Medium-',
                      color: Color.fromRGBO(71, 77, 96, 1.0),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 20.0, top: 10, bottom: 20),
              alignment: Alignment.centerLeft,
              child: Text(
                model.address,
                maxLines: 1,
                style: TextStyle(
                    fontFamily: 'FiraSans-Medium-',
                    color: Color.fromRGBO(71, 77, 96, 1.0),
                    fontSize: 17),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10),
              alignment: Alignment.centerLeft,
              child: Text(
                'GỢI Ý CHO BẠN!',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: FutureBuilder<ItemModel>(
                future: widget.bloc.fetAllRoom(),
                builder:
                    (BuildContext context, AsyncSnapshot<ItemModel> snapshot) {
                  if (snapshot.hasData) {
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      children: List<Widget>.generate(
                        snapshot.data.results.length,
                        (index) => _generateListRoomRecomand(
                            snapshot.data.results[index]),
                      ),
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ),
            FutureBuilder<ItemModel>(
              future: widget.bloc.fetAllRoom(),
              builder:
                  (BuildContext context, AsyncSnapshot<ItemModel> snapshot) {
                if (!snapshot.hasData) {
                  return Container(
                      margin: EdgeInsets.only(top: 50.0),
                      child: TileShimmerPage());
                } else {
                  return Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'PHÒNG MỚI',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                      ),
                      Column(
                        children: List<Widget>.generate(
                            snapshot.data.results.length,
                            (index) =>
                                _buildNewRoom(snapshot.data.results[index])),
                      ),
                    ],
                  );
                }
              },
            )
          ],
        ));
  }
}
