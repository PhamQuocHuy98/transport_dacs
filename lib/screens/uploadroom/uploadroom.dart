import 'package:dacs/blocs/uploadroombloc/bloc_done.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_infomation.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_location.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_room.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_upload.dart';
import 'package:dacs/components/ui/builddone.dart';
import 'package:dacs/components/ui/buildinfomation.dart';
import 'package:dacs/components/ui/buildlocation.dart';
import 'package:dacs/components/ui/buildroom.dart';
import 'package:dacs/components/ui/stepper.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/models/room.dart';

import 'package:dacs/screens/home/Home.dart';
import 'package:dacs/screens/home/Main.dart';
import 'package:dacs/screens/login_signup/success.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ScreenUploadRoom extends StatefulWidget {
  ScreenUploadRoom({Key key}) : super(key: key);

  _ScreenUploadRoomState createState() => _ScreenUploadRoomState();
}

class _ScreenUploadRoomState extends State<ScreenUploadRoom> {
  int index = 0;
  int currentIndex = 0;
  List<Widget> _children = [];
  LocationBloc locationBloc;
  InfomationBloc infomationBloc;
  RoomBloc roomBloc;
  DoneBloc doneBloc;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  UploadRoom uploadRoomBloc;
  Room room = new Room();
  @override
  void initState() {
    super.initState();
    locationBloc = new LocationBloc(
        'Ví dụ 300 Nguyễn Đình Chiểu', '', '', '', '0', '0', '0');
    uploadRoomBloc = new UploadRoom(room);
    infomationBloc = InfomationBloc(1, 1, 0, null);
    roomBloc = RoomBloc([]);
    doneBloc = DoneBloc(null, null, null);

    _children = [
      BuildLocaTion(),
      BuildInfo(),
      BuildRoom(),
      BuildDone(),
    ];
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LocationBloc>(
          builder: (_) => locationBloc,
        ),
        ChangeNotifierProvider<InfomationBloc>(
          builder: (_) => infomationBloc,
        ),
        ChangeNotifierProvider<RoomBloc>(
          builder: (_) => roomBloc,
        ),
        ChangeNotifierProvider<DoneBloc>(builder: (_) => doneBloc),
      ],
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          leading: Icon(null),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: Container(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                'Hủy',
                style: TextStyle(color: Colors.grey, fontSize: 15),
              ),
            ),
          ),
        ),
        body: Container(
          width: Dimension.getWidth(1.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Divider(
                  height: 1.0,
                ),
                Container(
                  child: StepperRoom(
                    currentIndex: currentIndex,
                  ),
                ),
                Divider(
                  height: 1.0,
                ),
                _children[index],
                SizedBox(
                  height: 50,
                ),
                CheckButton(
                  color: Colors.blue,
                  height: 50,
                  width: Dimension.getWidth(0.8),
                  title: 'Tiếp Theo',
                  onPressed: () {
                    if (index == 0) {
                      if (locationBloc.checkValidator(
                              locationBloc.getProvince(),
                              locationBloc.getDistrict(),
                              locationBloc.getWard(),
                              locationBloc.getAddress()) ==
                          false) {
                        _scaffoldKey.currentState.showSnackBar(
                          SnackBar(
                            content: Text('Dữ liệu chưa hợp lệ'),
                          ),
                        );
                        return;
                      }
                    } else if (index == 1) {
                      if (infomationBloc.checkValidator(
                              infomationBloc.getCapacity(),
                              infomationBloc.getPrice()) ==
                          false) {
                        _scaffoldKey.currentState.showSnackBar(
                          SnackBar(
                            content: Text('Dữ liệu chưa hợp lệ'),
                          ),
                        );
                        return;
                      }
                    } else if (index == 2) {
                      if (roomBloc.getLength() < 1 ||
                          roomBloc.getLength() == null) {
                        _scaffoldKey.currentState.showSnackBar(
                          SnackBar(
                            content: Text('Dữ liệu chưa hợp lệ'),
                          ),
                        );
                        return;
                      }
                    } else if (index == 3) {
                      if (doneBloc.checkVaidator(doneBloc.getPhone(),
                              doneBloc.getTitle(), doneBloc.getDescription()) ==
                          false) {
                        _scaffoldKey.currentState.showSnackBar(
                          SnackBar(
                            content: Text('Dữ liệu chưa hợp lệ'),
                          ),
                        );
                        return;
                      }
                    }

                    setState(() {
                      if (index < 3) {
                        index += 1;
                        currentIndex += 1;
                      } else if (index == 3) {
                        showDialog(
                            context: context,
                            child: AlertDialog(
                              content: Container(
                                width: 100,
                                height: 200,
                                child: Column(
                                  children: <Widget>[
                                    Text('Xác nhận đăng tải phòng',
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        )),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text('Bạn chắc chắn muốn đăng tải phòng ?'),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    CheckButton(
                                      title: 'Đồng ý',
                                      color: Colors.blue,
                                      width: 300,
                                      height: 40,
                                      onPressed: () async {
                                        room = new Room(
                                          address: locationBloc
                                              .getAddress()
                                              .toString(),
                                          district: locationBloc
                                              .getDistrict()
                                              .toString(),
                                          ward:
                                              locationBloc.getWard().toString(),
                                          province: locationBloc
                                              .getProvince()
                                              .toString(),
                                          provinceid: int.parse(locationBloc
                                              .getProvinceId()
                                              .toString()),
                                          districtid: int.parse(locationBloc
                                              .getDistrictId()
                                              .toString()),
                                          wardid: int.parse(locationBloc
                                              .getWardId()
                                              .toString()),
                                          categoryid: int.parse(infomationBloc
                                              .getCheckInfoRoom()
                                              .toString()),
                                          capacity: int.parse(infomationBloc
                                              .getCapacity()
                                              .toString()),
                                          sex: int.parse(infomationBloc
                                              .getSex()
                                              .toString()),
                                          price: double.parse(infomationBloc
                                              .getPrice()
                                              .toString()),
                                          lstRoom: roomBloc.getAllRoomImage(),
                                          phone: doneBloc.getPhone().toString(),
                                          name: doneBloc.getTitle().toString(),
                                          description: doneBloc
                                              .getDescription()
                                              .toString(),
                                          userid: await uploadRoomBloc.getId(),
                                        );
                                        uploadRoomBloc = new UploadRoom(room);
                                        // print('null ?'+room.userid.toString());
                                        await uploadRoomBloc
                                            .uploadRoom()
                                            .then((value) {
                                          if (value == 200) {
                                            setState(() {
                                              currentIndex += 1;
                                            });
                                            Navigator.pop(context);

                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => Success(
                                                      subtitle:
                                                          'Đăng tải phòng thành công',
                                                      titleButton:
                                                          'Về Trang Chủ',
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                MainScreen(),
                                                          ),
                                                        );
                                                      },
                                                    ),
                                              ),
                                            );
                                          } else {
                                            _scaffoldKey.currentState
                                                .showSnackBar(SnackBar(
                                              content: Text(
                                                  'Đã xảy ra lỗi trong quá trình upload'),
                                            ));
                                          }
                                        });
                                      },
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text('Hủy',
                                          style: TextStyle(
                                            color: Colors.blue,
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                            ));
                      }
                    });
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                MaterialButton(
                  onPressed: () {
                    if (index > 0) {
                      setState(() {
                        index -= 1;
                        currentIndex -= 1;
                      });
                    }
                  },
                  child: Text(
                    'Quay lại',
                    style: TextStyle(color: Colors.blue),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
