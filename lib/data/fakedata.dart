import 'package:dacs/data/images.dart';
import 'package:dacs/models/Splash.dart';
import 'package:dacs/models/room.dart';

List<Splash> lstSplash = [
  Splash(
      title: 'Tìm Kiếm Nơi Ở Du Lịch',
      subtitle: 'Tìm nơi ở du lịch uy tín & Mọi người cùng chia sẻ ngôi nhà',
      image: Images.imgArt1),
  Splash(
      title: 'Phù Hợp Với Sở Thích Của Bạn',
      subtitle: 'Cho chúng tôi biết sở thích của bạn và phù hợp với người',
      image: Images.imgArt2),
  Splash(
      title: 'Đăng Tải Phòng Cho Thuê ',
      subtitle: 'Bạn hãy đăng thông tin phòng bạn muốn cho người khác thuê',
      image: Images.imgArt3),
];

List<Room> lstRooms = [
  //Room(address: '')
  /*Room(name: 'Phòng cho thuê Trần kế xương gần quận 1', price: 200000, address: '303 Trần Kế Xương Quận 1 TP HCM',, image: Images.imgItemRoom),
  Room(name: 'Kí túc xá Hai Bà Trưng Quận 3', price: 2740000, address: '100 Hai Bà Trưng Quận 3 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Phòng cho thuê 33 Lý Phục Man', price: 3000000, address: '33 Lý Phục Man Quận 7 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Phòng Cho Thuê Cao Lỗ Quận 8', price: 4000000, address: '170/12 Cao Lỗ Phường 4 Quận 8 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Phòng ở ghép Trần Xuân Soạn Quận 7', price: 1300000, address: '675/33 Trần Xuân Soạn Quận 7 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Căn hộ mini đầy đủ nội thất Bình Thạnh', price: 500000, address: '220/76B Xô Viết Nghệ Tĩnh Quận Bình Thạnh', image: Images.imgItemRoom),


  Room(name: 'Phòng cho thuê Trần kế xương gần quận 1', price: 200000, address: '303 Trần Kế Xương Quận 1 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Kí túc xá Hai Bà Trưng Quận 3', price: 2740000, address: '100 Hai Bà Trưng Quận 3 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Phòng cho thuê 33 Lý Phục Man', price: 3000000, address: '33 Lý Phục Man Quận 7 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Phòng Cho Thuê Cao Lỗ Quận 8', price: 4000000, address: '170/12 Cao Lỗ Phường 4 Quận 8 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Phòng ở ghép Trần Xuân Soạn Quận 7', price: 1300000, address: '675/33 Trần Xuân Soạn Quận 7 TP HCM', image: Images.imgItemRoom),
  Room(name: 'Căn hộ mini đầy đủ nội thất Bình Thạnh', price: 500000, address: '220/76B Xô Viết Nghệ Tĩnh Quận Bình Thạnh', image: Images.imgItemRoom),*/
];
