import 'package:dacs/models/user.dart';
import 'package:dacs/models/userprofile.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharePrefUtil {
  final String _token = 'token';
  final int _id = -1;
  final String _name = 'name';
  final String _phone = 'phone';
  final String _avatar = 'avatar';
  Future<String> getToken() async {
    try {
      final sharedPreference = await SharedPreferences.getInstance();
      return sharedPreference.getString(_token);
    } catch (e) {
      print(e);
    }
  }

  Future<int> getUserId() async {
    try {
      final sharedPreference = await SharedPreferences.getInstance();
      return sharedPreference.getInt(_id.toString());
    } catch (e) {
      print(e);
    }
  }

  Future<UserProfile> getUserProfle() async {
    try {
      final sharedPreference = await SharedPreferences.getInstance();
      UserProfile user = new UserProfile();
      user.id = sharedPreference.getInt(_id.toString());
      user.avatar = sharedPreference.getString(_avatar);
      user.name = sharedPreference.getString(_name);
      user.phone = sharedPreference.getString(_phone);
      //return sharedPreference.getString(_token);
      return user;
    } catch (e) {
      print(e);
    }
  }

  Future<void> saveProfile(
      String token, int id, String name, String phone, String avatar) async {
    bool result;

    try {
      final sharePreference = await SharedPreferences.getInstance();
      result = await sharePreference.setString(_token, token);
      result = await sharePreference.setInt(_id.toString(), id);
      result = await sharePreference.setString(_name, name);
      result = await sharePreference.setString(_phone, phone);
      result = await sharePreference.setString(_avatar, avatar);
    } catch (e) {}
  }

  Future<void> deleteToken() async {
    bool result;
    try {
      final sharePreference = await SharedPreferences.getInstance();
      result = await sharePreference.remove(_token);
    } catch (e) {}
  }
}
