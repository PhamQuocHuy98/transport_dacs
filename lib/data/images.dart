class Images {
  static String imgSlider1 = "assets/images/slide1.png";
  static String imgSlider2 = "assets/images/slide2.png";
  static String imgSlider3 = "assets/images/slide3.png";
  static String imgSlider4 = "assets/images/slide4.png";

  static String imgArt1 = "assets/images/Art_1.png";
  static String imgArt2 = "assets/images/Art_2.png";
  static String imgArt3 = "assets/images/Art_3.png";

  static String imgSuccess = "assets/images/ic_success.png";
  static String imgRooms = "assets/images/ic_room.png";
  static String imgPlace = "assets/images/ic_entire_place.png";
  static String imgTenant = "assets/images/ic_tenant.png";
  static String imgRoomates = "assets/images/ic_roomates.png";

  static String imgItemRoom = "assets/images/anh3.jpg";
  static String imgBg = "assets/images/anh3.jpg";
}
