import 'package:flutter/material.dart';

class ColorApp {
  static Color mainColor = Color.fromRGBO(11, 188, 238, 1.0);
}

class SizeText {
  static double queryData = 0.0;
  static double size12 = 12.0 / queryData;
  static double size14 = 14.0 / queryData;
  static double size16 = 16.0 / queryData;
  static double size18 = 18.0 / queryData;
  static double size20 = 20.0 / queryData;
  static double size22 = 22.0 / queryData;
  static double size24 = 24.0 / queryData;
}

class Sizes {
  //static const sizeIconBottom = 24.0;
}

class StylesText {
  static TextStyle style14WhiteMontserratMedium = TextStyle(
      fontSize: SizeText.size14,
      color: Colors.white,
      fontFamily: 'Montserrat-Medium');

  static TextStyle style24BlackMontserratBold = TextStyle(
    fontSize: SizeText.size24,
    color: Color.fromRGBO(2, 4, 51, 1.0),
    fontWeight: FontWeight.bold,
  );
  static TextStyle style24WhiteMontserratBold = TextStyle(
    fontSize: SizeText.size24,
    color: Color.fromRGBO(255, 255, 255, 1.0),
    fontWeight: FontWeight.bold,
  );
  static TextStyle style20GrayMontserratRegular = TextStyle(
    fontSize: SizeText.size20,
    color: Color.fromRGBO(64, 75, 105, 1.0),
  );

  static TextStyle style16GrayMontserratRegular = TextStyle(
    fontSize: SizeText.size16,
    color: Color.fromRGBO(64, 75, 105, 1.0),
  );
  static TextStyle style18WhiteBalooBhai = TextStyle(
      fontSize: SizeText.size14,
      color: Colors.white,
      fontFamily: 'BalooBhai-Regular');

  static TextStyle style14WhiteBalooBhai = TextStyle(
      fontSize: SizeText.size20,
      color: Colors.white,
      fontFamily: 'BalooBhai-Regular');
  static TextStyle style18WhiteVarelaRound = TextStyle(
      fontSize: SizeText.size18,
      color: Colors.white,
      fontFamily: 'VarelaRound-Regular');

  static TextStyle style22Black = TextStyle(
      fontSize: SizeText.size22,
      color: Colors.black,
      fontWeight: FontWeight.bold);
}
