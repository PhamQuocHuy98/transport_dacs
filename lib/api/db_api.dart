import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:dacs/data/local/shared_pref_until.dart';
import 'package:dacs/models/district.dart';
import 'package:dacs/models/province.dart';
import 'package:dacs/models/room.dart';
import 'package:dacs/models/searchSuggestion.dart';
import 'package:dacs/models/user.dart';
import 'package:dacs/models/userprofile.dart';
import 'package:dacs/models/ward.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:dacs/data/fakedata.dart';
import 'package:dio/dio.dart';
import 'package:path/path.dart';

class DbApi {
  Future<int> registerUser(User user) async {
    String url = Url.baseURL;
    final response = await http.post('$url/register', body: {
      'name': user.name,
      'email': user.email,
      'password': user.password,
      'phone': user.phone,
    });
    print(response.body);
    return response.statusCode;
  }

  Future<int> loginUser(String email, String password) async {
    String url = Url.baseURL;
    final response = await http.post('$url/login', body: {
      'email': email,
      'password': password,
    });
    // print(response.body);
    SharePrefUtil share = new SharePrefUtil();
    if (response.body.toString() == "false") {
      return 404;
    }
    // print(json.decode(response.body)['id']);
    // print(response.body);
    share.saveProfile(
      json.decode(response.body)['token'],
      json.decode(response.body)['id'],
      json.decode(response.body)['name'],
      json.decode(response.body)['phone'],
      json.decode(response.body)['avatar'],
    );
    print(response.body);
    return 200;
  }

  Future<int> resetPassword(String email) async {
    String url = Url.baseURL;
    final response = await http.post('$url/reset-password', body: {
      'email': email,
    });
    return response.statusCode;
  }

  Future<int> updatePassword(String token, String password) async {
    String url = Url.baseURL;
    final response = await http.post('$url/update-password', body: {
      'token': token,
      'password': password,
    });
    print(response.statusCode);
    return response.statusCode;
  }

  Future<List<Room>> fetchFakeData() {
    List<Room> res = new List<Room>();
    res.addAll(lstRooms);

    return Future.delayed(new Duration(milliseconds: 3000), () => res);
  }

  Future<List<Province>> fetchAllProvince() async {
    List<Province> lstProvince = new List<Province>();
    String url = Url.baseURL;
    final response = await http.get('$url/provinces');
    if (response.statusCode == 200) {
      //print('laays dc roi');
      lstProvince = (json.decode(response.body) as List)
          .map((data) => new Province.fromJson((data)))
          .toList();
      // print(lstProvince.map((f) => f.name));
      return lstProvince;
    } else {
      throw Exception('Loi');
    }
  }

  Future<List<District>> fetchDistrictById(String id) async {
    List<District> lstDistrict = new List<District>();
    String url = Url.baseURL;
    final response = await http.get('$url/districts/$id');

    if (response.statusCode == 200) {
      lstDistrict = (json.decode(response.body) as List)
          .map((data) => new District.fromJson((data)))
          .toList();

      return lstDistrict;
    } else {
      throw Exception('Loi');
    }
  }

  Future<List<Ward>> fetchWardById(String id) async {
    List<Ward> lstWard = new List<Ward>();
    String url = Url.baseURL;
    final response = await http.get('$url/wards/$id');

    if (response.statusCode == 200) {
      lstWard = (json.decode(response.body) as List)
          .map((data) => new Ward.fromJson((data)))
          .toList();

      return lstWard;
    } else {
      throw Exception('Loi');
    }
  }

  Future<int> uploadRoom(Room room) async {
    var dio = new Dio();
    List<UploadFileInfo> lstfile = [];
    for (int i = 0; i < room.lstRoom.length; i++) {
      String name = basename(room.lstRoom[i].path);

      lstfile.add(UploadFileInfo(room.lstRoom[i], name));
    }

    // print(lstfile.length);
    FormData formData = new FormData.from({
      "province": room.province,
      "district": room.district,
      "ward": room.ward,
      "address": room.address,
      "name": room.name,
      "sex": room.sex.toString(),
      "phone": room.phone,
      "price": room.price.toString(),
      "capacity": room.capacity.toString(),
      "category_id": room.categoryid.toString(),
      "province_id": room.provinceid.toString(),
      "district_id": room.districtid.toString(),
      "ward_id": room.wardid.toString(),
      "user_id": room.userid.toString(),
      "description": room.description.toString(),
      "image[]": lstfile,
    });
    //  print('PAth' + basename(room.lstRoom[0].path));
    // print('file' + room.lstRoom[0].toString());
    String url = Url.baseURL;
    Response response;
    try {
      response = await dio.post('$url/image',
          data: formData,
          options: Options(
              method: 'post',
              contentType: ContentType.parse('multipart/form-data')));
    } catch (e) {
      print((e as DioError).response.data);
      // print('Lỗi' + e.toString());
    }
    print(response.data);
    return response.statusCode;
  }

  Future<ItemModel> fetchAllRoom() async {
    String url = Url.baseURL;
    final response = await http.get('$url/allroom/');
    ItemModel lstRoom;
    if (response.statusCode == 200) {
      lstRoom = ItemModel.fromJson(json.decode(response.body));

      //print('ok');
    } else {
      throw Exception('Lỗi');
    }
    //print(lstRoom.results.length);
    return lstRoom;
  }

  Future<ItemModel> fetchsearch(String text) async {
    print(text);
    String url = Url.baseURL;
    final response = await http.post('$url/searchroom', body: {
      'text': text,
    });
    ItemModel lstRoom;
    // print(response.body);
    try {
      if (response.statusCode == 200) {
        lstRoom = ItemModel.fromJson(json.decode(response.body));

        //print('ok');
      } else {
        //  throw Exception('Lỗi');
      }
    } catch (e) {
      print(e);
    }

    //print(lstRoom.results[0].phone.toLowerCase());
    return lstRoom;
  }

  Future<UserProfile> getProfilebyId(String id) async {
    String url = Url.baseURL;
    try {
      print('AA');
      final response = await http.get('$url/profile/$id');
      UserProfile user;
      // print(json.decode(response.body));
      if (response.statusCode == 200) {
        user = UserProfile.fromJson(json.decode(response.body));
      }
      print(user);
    } catch (e) {}
  }

  /*Future<Suggestion> getSuggestions(String text) async {
    String url = Url.baseURL;
    try {
      print('AA');
      final response =
          await http.post('$url/suggestions', body: {'text': text});
      Suggestion sug = new Suggestion();
      // print(json.decode(response.body));
      if (response.statusCode == 200) {
        sug = Suggestion.fromJson(json.decode(response.body));
      }
      print(sug.text);
      print(sug);
      return sug;
    } catch (e) {}
  }*/
}

DbApi api = DbApi();
