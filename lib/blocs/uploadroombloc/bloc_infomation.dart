import 'package:flutter/material.dart';

class InfomationBloc with ChangeNotifier {
  int _checkinforoom; //
  int _capacity; // sức chứa
  int _sex;
  double price;

  InfomationBloc(
    this._capacity,
    this._checkinforoom,
    this._sex,
    this.price,
  );

  int getCheckInfoRoom() => _checkinforoom;
  int getCapacity() => _capacity;
  int getSex() => _sex;
  double getPrice() => price;

  setCheckInfoRoom(int value) {
    _checkinforoom = value;
    notifyListeners();
  }

  setCapacity(int value) {
    _capacity = value;
    notifyListeners();
  }

  setSex(int value) {
    _sex = value;
    notifyListeners();
  }

  setPrice(double value) {
    price = value;
    notifyListeners();
  }

  checkValidator(int capacity, double price) {
    if (capacity == null || price == null) {
      return false;
    }
    return true;
  }
}
