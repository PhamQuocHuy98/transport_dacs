import 'package:dacs/api/db_api.dart';
import 'package:dacs/data/local/shared_pref_until.dart';
import 'package:dacs/models/room.dart';

class UploadRoom {
  Room _room;
  UploadRoom(this._room);

  Future<int> getId() async{
    int temp;
    SharePrefUtil share = new SharePrefUtil();
   await share.getUserId().then((value) {
      temp = value;
    });
    print(temp.toString());
    return temp;
  }

  Future<int> uploadRoom() async {
    int temp;
    await api.uploadRoom(_room).then((res) {
      //print('RES' + res.toString());
      //return res;
      temp = res;
    });
    return temp;
  }
}
