import 'package:flutter/material.dart';

class DoneBloc with ChangeNotifier {
  String _phone;
  String _title;
  String _description;

  DoneBloc(this._description, this._phone, this._title);

  getPhone() => _phone;
  getTitle() => _title;
  getDescription() => _description;

  setPhone(String value) {
    _phone = value;
    notifyListeners();
  }

  setTitle(String value) {
    _title = value;
    notifyListeners();
  }

  setDescription(String value) {
    _description = value;
    notifyListeners();
  }

  checkVaidator(String phone, String title, String description) {
    if (phone == null ||
        phone == '' ||
        title == '' ||
        title == null ||
        description == null ||
        description == '') {
      return false;
    }
    return true;
  }
}
