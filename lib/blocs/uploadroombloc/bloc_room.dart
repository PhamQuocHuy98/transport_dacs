import 'dart:io';

import 'package:flutter/material.dart';

class RoomBloc with ChangeNotifier {
  List<File> _listRoomImage = new List<File>();

  RoomBloc(
    this._listRoomImage,
  );

  getItemRoomImage(int index) => _listRoomImage[index];
  List<File> getAllRoomImage() => _listRoomImage;
  int getLength() => _listRoomImage.length;
  addListRoomImage(File image) {
    _listRoomImage.add(image);
    notifyListeners();
  }

  removeListRoomImage(int index) {
    _listRoomImage.removeAt(index);
    notifyListeners();
  }
}
