import 'package:flutter/material.dart';

class LocationBloc with ChangeNotifier {
  String _province;
  String _district;
  String _ward;
  String _address;
  String _province_id;
  String _district_id;
  String _ward_id;
  LocationBloc(this._address, this._district, this._province, this._ward,
      this._province_id, this._district_id, this._ward_id);

  getProvince() => _province;
  getProvinceId() => _province_id;
  getDistrict() => _district;
  getDistrictId() => _district_id;
  getWard() => _ward;
  getWardId() => _ward_id;
  getAddress() => _address;

  setProvince(String province, String province_id) {
    _province = province;
    _province_id = province_id;
    notifyListeners();
  }

  setDistrict(String district, String district_id) {
    _district = district;
    _district_id = district_id;
    notifyListeners();
  }

  setWard(String ward, String ward_id) {
    _ward = ward;
    _ward_id = ward_id;
    notifyListeners();
  }

  setAddress(String address) {
    _address = address;
    notifyListeners();
  }

  checkValidator(
      String province, String district, String ward, String address) {
    if (province == '' || district == '' || ward == '' || address == '') {
      return false;
    }
    return true;
  }
}
