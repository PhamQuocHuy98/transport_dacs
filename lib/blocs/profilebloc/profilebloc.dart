import 'package:dacs/api/db_api.dart';
import 'package:dacs/models/userprofile.dart';

import 'package:flutter/material.dart';

class ProFileBloc with ChangeNotifier {
  Future<UserProfile> getProfileByID(String id) async {
    UserProfile user;
    await api.getProfilebyId(id).then((value) {
      //notifyListeners();
      user = value;
    });
    return user;
  }
}
