import 'dart:async';
import 'package:dacs/api/db_api.dart';
import 'package:dacs/blocs/bloc_provider.dart';
import 'package:dacs/models/user.dart';
import 'package:dacs/screens/setup/setupprofile.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends Object implements BlocBase {
  final BehaviorSubject<String> _emailController =
      new BehaviorSubject<String>();
  final BehaviorSubject<String> _passwordController =
      new BehaviorSubject<String>();

  // check input
  Function(String) get onEmailChanged => _emailController.sink.add;
  Function(String) get onPasswordChanged => _passwordController.sink.add;

  //validator
  StreamTransformer<String, String> validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    if (Validator.isValidEmail(email) == false) {
      sink.addError('Email không hợp lệ');
    } else {
      sink.add(email);
    }
  });

  StreamTransformer<String, String> vadidatePassword =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (password, sink) {
    if (Validator.isValidPassword(password) == false) {
      sink.addError('Mật khẩu không hợp lệ');
    } else
      sink.add(password);
  });

  Stream<String> get email => _emailController.stream.transform(validateEmail);

  Stream<String> get password =>
      _passwordController.stream.transform(vadidatePassword);

  // Login button
  Stream<bool> get loginValid =>
      Observable.combineLatest2(email, password, (e, p) => true);

  Future<int> submit(BuildContext context) async {
    int temp;
    await api
        .loginUser(_emailController.value, _passwordController.value)
        .then((val) {
      // val == 200
      //     ? Navigator.push(
      //         context, MaterialPageRoute(builder: (context) => SetupProfile()))
      //     : null;
      temp = val;
    });
    //print(temp);
    return temp;
  }

  @override
  void dispose() {
    _emailController.close();
    _passwordController.close();
  }
}

class RegisterBloc extends Object implements BlocBase {
  final BehaviorSubject _emailController = new BehaviorSubject<String>();
  final BehaviorSubject _passwordController = new BehaviorSubject<String>();
  final BehaviorSubject _phoneController = new BehaviorSubject<String>();
  final BehaviorSubject _nameController = new BehaviorSubject<String>();
  //event

  Function(String) get onEmailChanged => _emailController.sink.add;
  Function(String) get onPasswordChanged => _passwordController.sink.add;
  Function(String) get onPhoneChanged => _phoneController.sink.add;
  Function(String) get onNameChanged => _nameController.sink.add;
  // add

  Stream<String> get email =>
      _emailController.stream.transform(_validatorEmail);
  Stream<String> get password =>
      _passwordController.stream.transform(_validatorPassword);
  Stream<String> get phone =>
      _phoneController.stream.transform(_validatorPhone);
  Stream<String> get name => _nameController.stream.transform(_validatorName);

  StreamTransformer<String, String> _validatorEmail =
      StreamTransformer.fromHandlers(handleData: (email, sink) {
    if (Validator.isValidEmail(email) == false) {
      sink.addError('Email không hợp lệ');
    } else
      sink.add(email);
  });

  StreamTransformer<String, String> _validatorName =
      StreamTransformer.fromHandlers(handleData: (name, sink) {
    if (Validator.isValidName(name) == false) {
      sink.addError('Tên không hợp lệ');
    } else {
      sink.add(name);
    }
  });

  StreamTransformer<String, String> _validatorPassword =
      StreamTransformer.fromHandlers(handleData: (password, sink) {
    if (Validator.isValidPassword(password) == false) {
      sink.addError('Password không hợp lệ');
    } else {
      sink.add(password);
    }
  });

  StreamTransformer<String, String> _validatorPhone =
      StreamTransformer.fromHandlers(handleData: (phone, sink) {
    if (Validator.isValidPhone(phone) == false) {
      sink.addError('Điện thoại không hợp lệ');
    } else {
      sink.add(phone);
    }
  });

  Stream<bool> get registerValid => Observable.combineLatest4(
      name, email, password, phone, (n, e, p, ph) => true);

  Future<int> onSubmit() async {
    User user = new User();
    user.Name = _nameController.value;
    user.phone = _phoneController.value;
    user.email = _emailController.value;
    user.password = _passwordController.value;
    int temp;
    await api.registerUser(user).then((res) {
      temp = res;
    });
    return temp;
  }

  @override
  void dispose() {
    _nameController.close();
    _emailController.close();
    _passwordController.close();
    _phoneController.close();
  }
}

class DrawBloc extends Object implements BlocBase {
  //BehaviorSubject _drawController = new BehaviorSubject<bool>();

  //Function()

  @override
  void dispose() {}
}

class ForgotPassWordBloc extends Object implements BlocBase {
  final BehaviorSubject _emailController = new BehaviorSubject<String>();

  Function(String) get onEmailChanged => _emailController.sink.add;

  Stream<String> get email =>
      _emailController.stream.transform(_validatorEmail);

  StreamTransformer<String, String> _validatorEmail =
      StreamTransformer.fromHandlers(handleData: (email, sink) {
    if (Validator.isValidEmail(email) == false) {
      sink.addError('Email không hợp lệ');
    } else
      sink.add(email);
  });

  Stream<bool> get submitValid =>
      Observable.combineLatest2(email, email, (e, em) => true);

  Future<int> onSubmit() async {
    int temp;
    await api.resetPassword(_emailController.value).then((val) {
      temp = val;
    });
    return temp;
  }

  @override
  void dispose() {
    _emailController.close();
  }
}

class UpdatePasswordBloc extends Object implements BlocBase {
  final BehaviorSubject _tokenController = new BehaviorSubject<String>();
  final BehaviorSubject _passwordController = new BehaviorSubject<String>();
  final BehaviorSubject _confirmController = new BehaviorSubject<String>();

  Function(String) get onTokenChanged => _tokenController.sink.add;
  Function(String) get onPasswordChanged => _passwordController.sink.add;
  Function(String) get onConfirmPasswordChanged => _confirmController.sink.add;

  Stream<String> get token =>
      _tokenController.stream.transform(_validatorToken);
  Stream<String> get password =>
      _passwordController.stream.transform(_validatorPassword);
  Stream<String> get confirmpassword =>
      _confirmController.stream.transform(_validatorConfirmPassword);

  StreamTransformer<String, String> _validatorToken =
      StreamTransformer.fromHandlers(handleData: (token, sink) {
    if (Validator.isValidName(token) == false) {
      sink.addError('Token không hợp lệ');
    } else
      sink.add(token);
  });

  StreamTransformer<String, String> _validatorPassword =
      StreamTransformer.fromHandlers(handleData: (password, sink) {
    if (Validator.isValidPassword(password) == false) {
      sink.addError('Mật khẩu không hợp lệ');
    } else
      sink.add(password);
  });

  StreamTransformer<String, String> _validatorConfirmPassword =
      StreamTransformer.fromHandlers(handleData: (confirmpassword, sink) {
    if (Validator.isValidPassword(confirmpassword) == false) {
      sink.addError('Mật khẩu không hợp lệ');
    } else
      sink.add(confirmpassword);
  });
  Stream<bool> get submit => Observable.combineLatest3(
      token, password, confirmpassword, (t, p, c) => true);

  Future<int> onSubmit() async {
    int temp;
    //  print(_tokenController.value);
    if (_confirmController.value != _passwordController.value) {
      print('HAHAHA');
      return 404;
    } else {
      await api
          .updatePassword(_tokenController.value, _passwordController.value)
          .then((val) {
        temp = val;
      });
    }
    print(temp);
    return temp;
  }

  @override
  void dispose() {
    _confirmController.close();
    _passwordController.close();
    _tokenController.close();
  }
}
