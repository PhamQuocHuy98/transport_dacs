import 'package:dacs/api/db_api.dart';
import 'package:dacs/models/room.dart';
import 'package:flutter/material.dart';

class SearchBloc with ChangeNotifier {
  List<Result> _searchResult = [];
  List<Result> _roomDetails = [];
  String _textSearch = '';
  double _lowerValue;
  double _upperValue;
  String getTextSearch() => _textSearch;
  void setTextSearch(String value) {
    _textSearch = value;
    notifyListeners();
  }

  double getLowerValueSlider() => _lowerValue;
  void setLowerValueSlider(double value) {
    _lowerValue = value;
    notifyListeners();
  }

  double getUpperSlider() => _upperValue;
  void setUpperValueSlider(double value) {
    _upperValue = value;
    notifyListeners();
  }

  List<Result> getSearchResult() {
    return _searchResult;
  }

  List<Result> getroomDetails() => _roomDetails;

  SearchBloc(
    this._searchResult,
    this._roomDetails,
    this._textSearch,
    this._lowerValue,
    this._upperValue,
  );

  Future<List<Result>> fetchsearch(String text) async {
    _searchResult.clear();
    _roomDetails.clear();

    await api.fetchsearch(text).then((value) {
      for (Result room in value.results) {
        _searchResult.add(room);
      }
    });
    notifyListeners();
    _searchResult.sort((a, b) => a.price.compareTo(b.price));
    return _searchResult;
  }

  resetSearchResult() {
    _searchResult.clear();
    notifyListeners();
  }

  onSearchTextChanged(String text) async {
    text = text.toLowerCase();
    _roomDetails.clear();
    if (text.isEmpty) {
      notifyListeners();
      return;
    }

    _searchResult.forEach((searchResult) {
      if (searchResult.name.toLowerCase().contains(text) ||
          searchResult.province.toLowerCase().contains(text) ||
          searchResult.district.toLowerCase().contains(text) ||
          searchResult.ward.toLowerCase().contains(text) ||
          searchResult.address.toLowerCase().contains(text))
        _roomDetails.add(searchResult);
      print('Có luôn');
    });
    print('AAAAA');
    print(_roomDetails.length);
    notifyListeners();
  }

  onSearchSliderchanged(int handler, double lower, double upper) async {
    _roomDetails.clear();
    setLowerValueSlider(lower);
    setUpperValueSlider(upper);
    _searchResult.forEach((searchResult) {
      if (searchResult.price >= lower && searchResult.price <= upper)
        _roomDetails.add(searchResult);
    });
    print(lower);
    notifyListeners();
  }
}
