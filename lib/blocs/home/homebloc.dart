import 'package:dacs/api/db_api.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_location.dart';
import 'package:dacs/models/room.dart';
import 'package:flutter/material.dart';

class HomeBloc with ChangeNotifier {
  ItemModel item;

  Future<ItemModel> fetAllRoom() async {
    await api.fetchAllRoom().then((value) {
      //notifyListeners();
      return item = value;
    });
    return item;
  }

  getItemModel() => item;
}
