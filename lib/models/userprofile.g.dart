part of 'userprofile.dart';

UserProfile _$UserProfileFromJson(Map<String, dynamic> json) {
  print("???/");
  List<String> lst = new List<String>();
  for (int i = 0; i < json['rooms'].length; i++) {
    lst.add(json['rooms'][i]);
  }
  print('LIST');
  print(lst);
  return UserProfile(
    avatar: json['avartar'] as String,
    countView: json['rooms']['count_view'] as int,
    email: json['email'] as String,
    id: json['id'] as int,
    name: json['name'] as String,
    phone: json['phone'] as String,
    sumRoom: json['sumroom'] as int,
    lstImage: lst,
  );
}

Map<String, dynamic> _$UserProfileToJson(UserProfile instance) =>
    <String, dynamic>{
      'id': instance.id,
      'avatar': instance.avatar,
      'name': instance.name,
      'email': instance.email,
      'phone': instance.phone,
    };
