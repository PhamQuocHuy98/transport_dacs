class Suggestion {
  List<String> text;

  Suggestion({this.text});
  factory Suggestion.fromJson(Map<String, dynamic> parsedJson) {
    List<String> temp = [];
    for (int i = 0; i < parsedJson['text'].length; i++) {
      temp.add(parsedJson['text'][i]);
    }
    return Suggestion(text: temp);
  }
}
