part of 'province.dart';

Province _$ProvinceFromJson(Map<String, dynamic> json) {
  return Province(
    id: json['id'] as int,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$ProvinceToJson(Province instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
