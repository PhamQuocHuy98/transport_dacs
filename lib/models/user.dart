import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

class User {
  int id;
  String avatar;
  String name;
  String email;
  String password;
  String phone;

  @JsonKey(name: "is_active")
  final bool isActive;
  @JsonKey(name: "api_token")
  final String token;

  final int level;
  String get Name => name;
  set Name(String value) => name = value;
  String get Email => email;
  set Email(String value) => email = value;
  String get Password => password;
  set Password(String value) => password = value;
  String get Phone => phone;
  set Phone(String value) => phone = value;
  User(
      {this.level,
      this.id,
      this.token,
      this.avatar,
      this.email,
      this.name,
      this.isActive,
      this.phone,
      this.password});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
