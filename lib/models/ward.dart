part 'ward.g.dart';

class Ward {
  int id;
  String name;
  int district_id;
  Ward({this.id, this.name, this.district_id});

  factory Ward.fromJson(Map<String, dynamic> json) => _$WardFromJson(json);

  Map<String, dynamic> toJson() => _$WardToJson(this);
}
