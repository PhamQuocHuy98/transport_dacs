class Splash {
  String image;
  String title;
  String subtitle;

  Splash({
    this.image,
    this.title,
    this.subtitle,
  });
}
