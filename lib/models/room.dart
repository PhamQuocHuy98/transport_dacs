import 'dart:convert';
import 'dart:io';

import 'package:json_annotation/json_annotation.dart';

//part 'room.g.dart';

/*class ItemRoom1 {
  List<Room> _result = [];
  ItemRoom1.fromJson(Map<String, dynamic> json) {
    List<Room> lstroom = [];
    for (int i = 0; i < json['result'].length; i++) {
      // print(json['result'][i]);

      Room room = Room.fromJson(json['result'][i]);
     // print('AA');
      lstroom.add(room);
    }
    _result = lstroom;
  }
  List<Room> get lstroom => _result;
}*/

class Room {
  @JsonKey(name: "id")
  int roomid;
  String province;
  @JsonKey(name: "province_id")
  int provinceid;
  String district;
  @JsonKey(name: "district_id")
  int districtid;
  String ward;
  @JsonKey(name: "ward_id")
  int wardid;
  String address;

  @JsonKey(name: "categoty")
  String category;

  @JsonKey(name: "categoty_id")
  int categoryid;
  int capacity;
  int sex;
  double price;
  @JsonKey(name: "user_id")
  int userid;

  @JsonKey(name: 'user_name')
  String username;
  List<File> lstRoom = new List<File>();
  String phone;
  String name;
  String description;
  @JsonKey(name: "images")
  List<String> image = new List<String>();

  Room(
      {this.provinceid,
      this.roomid,
      this.province,
      this.category,
      this.district,
      this.districtid,
      this.ward,
      this.wardid,
      this.address,
      this.categoryid,
      this.capacity,
      this.sex,
      this.price,
      this.lstRoom,
      this.phone,
      this.name,
      this.description,
      this.image,
      this.username,
      this.userid});
  //factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);

  //Map<String, dynamic> toJson() => _$RoomToJson(this);
}

class ItemModel {
  List<Result> _results = [];

  ItemModel.fromJson(Map<String, dynamic> parsedJson) {
    //print(parsedJson['results'].length);

    List<Result> temp = [];
    for (int i = 0; i < parsedJson['result'].length; i++) {
      // print('AA');
      Result result = Result(parsedJson['result'][i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<Result> get results => _results;
}

class Result {
  @JsonKey(name: "id")
  int roomid;
  String province;
  @JsonKey(name: "province_id")
  int provinceid;
  String district;
  @JsonKey(name: "district_id")
  int districtid;
  String ward;
  @JsonKey(name: "ward_id")
  int wardid;
  String address;

  @JsonKey(name: "categoty")
  String category;

  @JsonKey(name: "categoty_id")
  int categoryid;
  int capacity;
  int sex;
  double price;
  @JsonKey(name: "user_id")
  int userid;

  @JsonKey(name: 'user_name')
  String username;
  List<File> lstRoom = new List<File>();
  String phone;
  String name;
  String description;
  @JsonKey(name: "images")
  List<String> image = new List<String>();

  Result(json) {
    roomid = json['id'];
    address = json['address'];
    capacity = json['capacity'];
    categoryid = json['category_id'];
    description = json['description'];
    district = json['district'];
    districtid = json['district_id'];
    for (int i = 0; i < json['images'].length; i++) {
      // print('AA');
      image.add(json['images'][i]['image']);
    }

    name = json['name'];
    phone = json['phone'];
    price = double.parse(json['price'].toString());
    province = json['province'];
    provinceid = json['province_id'];
    sex = json['sex'];
    userid = json['user_id'];
    username = json['user_name'];
    ward = json['ward'];
    wardid = json['ward_id'];
    category = json['categoty'];
  }
}
