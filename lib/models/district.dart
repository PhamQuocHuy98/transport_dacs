part 'district.g.dart';

class District {
  int id;
  String name;
  int province_id;
  District({this.id, this.name, this.province_id});

  factory District.fromJson(Map<String, dynamic> json) =>
      _$DistrictFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictToJson(this);
}
