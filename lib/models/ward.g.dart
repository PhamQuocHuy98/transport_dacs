part of 'ward.dart';

Ward _$WardFromJson(Map<String, dynamic> json) {
  return Ward(
    id: json['id'] as int,
    name: json['name'] as String,
    district_id: json['district_id'] as int,
  );
}

Map<String, dynamic> _$WardToJson(Ward instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'district_id': instance.district_id
    };
