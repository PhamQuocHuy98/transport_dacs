part of 'district.dart';

District _$DistrictFromJson(Map<String, dynamic> json) {
  return District(
    id: json['id'] as int,
    name: json['name'] as String,
    province_id: json['province_id'] as int,
  );
}

Map<String, dynamic> _$DistrictToJson(District instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'province_id': instance.province_id
    };
