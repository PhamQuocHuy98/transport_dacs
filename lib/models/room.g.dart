/*part of 'room.dart';

Room _$RoomFromJson(Map<String, dynamic> json) {
  Room room = new Room();

  var lstImageFromJson = json['images'];
  List<String> lst = new List<String>();
  for (int j = 0; j < lstImageFromJson.length; j++) {
    lst.add(lstImageFromJson[j].toString());
  }

  // print(lst);
  room = new Room(
    roomid: json['id'] as int,
    address: json['address'] as String,
    capacity: json['capacity'] as int,
    categoryid: json['category_id'] as int,
    description: json['description'] as String,
    district: ['district'] as String,
    districtid: ['district_id'] as int,
    image: lst,
    name: json['name'] as String,
    phone: json['phone'] as String,
    price: double.parse(json['price'].toString()),
    province: json['province'] as String,
    provinceid: json['province_id'] as int,
    sex: json['sex'] as int,
    userid: json['user_id'] as int,
    username: json['user_name'] as String,
    ward: json['ward'] as String,
    wardid: json['ward_id'] as int,
    category: json['categoty'] as String,
  );

  return room;
}

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'id': instance.roomid,
      'address': instance.address,
      'capacity': instance.capacity,
      'category_id': instance.categoryid,
      'description': instance.description,
      'district': instance.district,
      'district_id': instance.districtid,
      'images': instance.image,
      'name': instance.name,
      'phone': instance.phone,
      'price': instance.price,
      'province': instance.province,
      'province_id': instance.provinceid,
      'sex': instance.sex,
      'user_id': instance.userid,
      'user_name': instance.username,
      'ward': instance.ward,
      'ward_id': instance.wardid,
      'categoty': instance.category,
    };*/
