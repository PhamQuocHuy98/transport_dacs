import 'dart:convert';

part 'userprofile.g.dart';

class UserProfile {
  int id;

  String name;
  String phone;
  String avatar;
  String email;
  int sumRoom;
  int countView;
  List<String> lstImage;
  UserProfile(
      {this.id,
      this.phone,
      this.name,
      this.avatar,
      this.email,
      this.countView,
      this.lstImage,
      this.sumRoom});
  factory UserProfile.fromJson(Map<String, dynamic> json) =>
      _$UserProfileFromJson(json);

  Map<String, dynamic> toJson() => _$UserProfileToJson(this);
}
