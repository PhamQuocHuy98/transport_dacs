part of 'user.dart';

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
      token: json['token'] as String,
      avatar: json['avatar'] as String,
      name: json['name'] as String,
      id: int.parse(json['id']),
      isActive: json['is_active'] as bool,
      email: json['email'] as String,
      password: json['password'] as String,
      level: int.parse(json['level']),
      phone: json['phone'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'avatar': instance.avatar,
      'token': instance.token,
      'name': instance.name,
      'is_active': instance.isActive,
      'email': instance.email,
      'password': instance.password,
      'level': instance.level,
      'phone': instance.phone,
    };
