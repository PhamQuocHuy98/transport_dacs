import 'package:dacs/screens/home/Home.dart';
import 'package:dacs/screens/home/Main.dart';
import 'package:dacs/screens/login_signup/fogotpassword.dart';
import 'package:dacs/screens/login_signup/login.dart';
import 'package:dacs/screens/login_signup/resetpassword.dart';
import 'package:dacs/screens/login_signup/signup.dart';
import 'package:dacs/screens/login_signup/success.dart';
import 'package:dacs/screens/splashs/Splashone.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route buildAuthorizedRoutes(RouteSettings settings) {
    switch (settings.name) {
      case "/login":
        return buildRoute(settings, Login());
      case "/signup":
        return buildRoute(settings, SignUp());
      case "/home":
        return buildRoute(settings, Home());
      case "/splash":
        return buildRoute(settings, Splash());
      case "/main":
        return buildRoute(settings, MainScreen());
      case "/fogot-password":
        return buildRoute(settings, FoGotPassWord());
      case "/reset-password":
        return buildRoute(settings, ResetPassWord());

      default:
        return null;
    }
  }

  static MaterialPageRoute buildRoute(RouteSettings settings, Widget builder) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => builder,
    );
  }

  static MaterialPageRoute buildDialog(RouteSettings settings, Widget builder) {
    return MaterialPageRoute(
        settings: settings,
        builder: (BuildContext context) => builder,
        fullscreenDialog: true);
  }
}
