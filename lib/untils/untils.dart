const LOG_ENABLED = true;

class Dimension {
  static double height = 0.0;
  static double witdh = 0.0;

  static double getWidth(double size) {
    return witdh * size;
  }

  static double getHeight(double size) {
    return height * size;
  }
}

class Utils {
  // Hàm này dùng để thay cho print('')
  static void printLog(String data) {
    if (LOG_ENABLED) {
      print(data);
    }
  }
}

class Validator {
  Validator._();

  static bool isValidPassword(String password) {
    if (password.length >= 8) return true;
    return false;
  }

  static bool isValidPhone(String phone) {
    if (phone.length < 10 || phone.length > 11) {
      return false;
    }
    return true;
  }

  static bool isValidName(String name) {
    if (name.isEmpty || name.length < 2) {
      return false;
    }
    return true;
  }

  static bool isValidEmail(String email) {
    final _emailRegExpString = r'[a-zA-Z0-9\+\.\_\%\-\+]{1,256}\@[a-zA-Z0-9]'
        r'[a-zA-Z0-9\-]{0,64}(\.[a-zA-Z0-9][a-zA-Z0-9\-]{0,25})+';
    return RegExp(_emailRegExpString, caseSensitive: false).hasMatch(email);
  }
}

class Url {
  static String baseURL = "http://10.224.167.84/dacs/public/api";
  //"http://10.0.2.2:8000/api";
}
