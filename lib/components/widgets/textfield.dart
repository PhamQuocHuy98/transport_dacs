import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String label;
  final Icon prefixIcon;
  final Icon suffixIcon;
  final Color colorSuffixIcon;
  final String errorText;

  final bool obscureText;
  final ValueChanged<String> onChange;
  CustomTextField(
      {this.label,
      this.prefixIcon,
      this.suffixIcon,
      this.errorText,
      this.onChange,
      this.obscureText = false,
      this.colorSuffixIcon = Colors.blue});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextField(
      decoration: InputDecoration(
        errorText: errorText,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon == null
            ? null
            : IconButton(
                onPressed: () {},
                icon: suffixIcon,
                color: colorSuffixIcon,
              ),
        enabledBorder: const OutlineInputBorder(
          // width: 0.0 produces a thin "hairline" border
          borderSide: const BorderSide(color: Colors.grey, width: 0.0),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        labelText: label,
      ),
      onChanged: onChange,
      obscureText: obscureText,
    );
  }
}
