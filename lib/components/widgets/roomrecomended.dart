import 'package:cached_network_image/cached_network_image.dart';
import 'package:dacs/components/widgets/starrating.dart';
import 'package:dacs/data/images.dart';
import 'package:flutter/material.dart';

class ImageCard extends StatelessWidget {
  final bigTitle;

  final subTitle;
  final image;

  final width;
  // final tag;

  ImageCard(
      {@required this.bigTitle,
      // @required this.tag,
      @required this.image,
      @required this.subTitle,
      this.width});
  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey[300],
                  offset: Offset(0.0, 10.0),
                  blurRadius: 10.0)
            ]),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          child: Column(
            children: <Widget>[
              CachedNetworkImage(
                width: 300,
                height: 170,
                imageUrl: image,
                placeholder: (context, url) => new Center(
                      child: CircularProgressIndicator(),
                    ),
                errorWidget: (context, url, error) => new Icon(Icons.error),
                fit: BoxFit.fitWidth,
              ),
              Container(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        bigTitle,
                        maxLines: 1,
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                      Padding(padding: EdgeInsets.all(5.0)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  StarRating(
                                    color: Colors.amber,
                                    rating: 4,
                                    starCount: 5,
                                    size: 20,
                                  ),
                                ],
                              ),
                              Text(
                                subTitle,
                                style: TextStyle(
                                    fontSize: 15.0, color: Colors.grey),
                              )
                            ],
                          ),
                          /*ClipRRect(
                            borderRadius: BorderRadius.circular(18.0),
                            child: Image.asset(
                             // imgAvatar,
                              fit: BoxFit.cover,
                              height: 40,
                              width: 40,
                            ),
                          ),*/
                        ],
                      )
                    ],
                  )),
            ],
          ),
        ));
  }
}

class ImageCardFake extends StatefulWidget {
  final double height;
  final double width;

  ImageCardFake({Key key, this.height = 20, this.width = 200})
      : super(key: key);

  createState() => ImageCardFakeState();
}

class ImageCardFakeState extends State<ImageCardFake>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  Animation gradientPosition;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        duration: Duration(milliseconds: 1500), vsync: this);

    gradientPosition = Tween<double>(
      begin: -3,
      end: 10,
    ).animate(
      CurvedAnimation(parent: _controller, curve: Curves.linear),
    )..addListener(() {
        setState(() {});
      });

    _controller.repeat();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10.0),
      width: widget.width,
      height: widget.height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          gradient: LinearGradient(
              begin: Alignment(gradientPosition.value, 0),
              end: Alignment(-1, 0),
              colors: [Colors.black12, Colors.black26, Colors.black12])),
    );
  }
}


