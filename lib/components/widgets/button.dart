import 'package:dacs/styles/styles.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CommonButton extends StatelessWidget {
  final String title;
  final GestureTapCallback onPressed;
  final Color startColor;

  final Color endColor;

  const CommonButton({
    Key key,
    this.onPressed,
    this.title,
    this.startColor = Colors.white,
    this.endColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: Dimension.getWidth(0.89),
        height: 60,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            gradient: LinearGradient(
              colors: [
                //Color.fromRGBO(15, 115, 238, 1.0), // xanh
                //Color.fromRGBO(198, 68, 252, 1.0), // tím
                startColor,
                endColor,
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            )),
        child: Center(
          child: Shimmer.fromColors(
             baseColor: Colors.white,
        highlightColor: Colors.black,
            child: Text(title, style: StylesText.style14WhiteBalooBhai),
          ),
        ),
      ),
    );
  }
}

class CheckButton extends StatelessWidget {
  final String title;
  final GestureTapCallback onPressed;
  final Color color;
  final bool hasIcon;

  final isClick;
  final Color colorTitle;
  final double width;
  final double height;
  const CheckButton({
    Key key,
    this.onPressed,
    this.title,
    this.color = Colors.white,
    this.hasIcon = false,
    this.isClick = false,
    this.colorTitle = Colors.white,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        //width: Dimension.getWidth(0.89),
        //height: 50,
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Center(
          child: Text(title,
              style: TextStyle(
                  fontSize: 14,
                  color: colorTitle,
                  fontFamily: 'Montserrat-Medium')),
        ),
      ),
    );
  }
}

class CusTomOutlineButtom extends StatelessWidget {
  final Color fillColor;
  final Color textColor;
  final Icon icon;
  final String title;
  final GestureTapCallback onPressed;
  final double width;
  final double height;
  final Color borderColor;
  final double borderRadius;
  final double borderWidth;
  const CusTomOutlineButtom({
    Key key,
    this.onPressed,
    this.icon,
    this.title,
    this.fillColor = Colors.blue,
    this.textColor = Colors.white,
    this.width = 150.0,
    this.height = 50,
    this.borderColor = Colors.transparent,
    this.borderRadius = 20.0,
    this.borderWidth = 1.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8),
        width: width,
        height: height,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            icon != null
                ? icon
                : SizedBox(
                    width: 0,
                  ),
            icon != null && title != null
                ? SizedBox(width: 10)
                : SizedBox(width: 0),
            title != null
                ? Text(
                    title,
                    style: TextStyle(
                      color: textColor,
                      fontSize: 16,
                    ),
                  )
                : SizedBox(
                    width: 0,
                  ),
          ],
        ),
      ),
      fillColor: fillColor,
      shape: OutlineInputBorder(
          borderSide: BorderSide(color: borderColor, width: borderWidth),
          borderRadius: BorderRadius.circular(borderRadius)),
      onPressed: onPressed,
    );
  }
}
