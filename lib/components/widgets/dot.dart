import 'package:flutter/material.dart';

class Dot extends StatelessWidget {
  final bool select;
  Dot({
    this.select = false,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 10,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        shape: BoxShape.circle,
        color:
            select == false ? Colors.white : Color.fromRGBO(15, 115, 238, 1.0),
      ),
      child: Text(''),
    );
  }
}
