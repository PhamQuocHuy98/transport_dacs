import 'package:dacs/styles/styles.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';

class PageWidget extends StatelessWidget {
  final String images;
  final String title;
  final String subtitle;
  PageWidget({
    this.images,
    this.title,
    this.subtitle,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
        //  width: Dimension.getWidth(1.0),
        child: Column(
      children: <Widget>[
        Image.asset(
          images,
          height: Dimension.getHeight(0.57),
          fit: BoxFit.fill,
        ),
        Container(
            margin: EdgeInsets.only(top: 15),
            child: Column(
              children: <Widget>[
                Text(
                  title,
                  style: StylesText.style24BlackMontserratBold,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  subtitle,
                  style: StylesText.style20GrayMontserratRegular,
                  textAlign: TextAlign.center,
                )
              ],
            )),
      ],
    ));
  }
}
