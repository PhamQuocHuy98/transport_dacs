import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  final String titleAppBar;
  final String imgBg;
  final String title;
  final String subtitle;
  final GestureTapCallback onPressed;
  final String textButton;

  const CustomAppBar(
      {Key key,
      this.titleAppBar = 'Categories',
      this.imgBg,
      this.title = 'Run Beyond Limits',
      this.subtitle = 'Our New Winter Collection is now available in stores',
      this.onPressed,
      this.textButton = 'View All'})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    return SliverAppBar(
      iconTheme: IconThemeData(color: Colors.white),
      expandedHeight: Dimension.getWidth(1.0),
      pinned: true,
      centerTitle: true,
      title: Text(
        titleAppBar,
        //style: ,
      ),
      flexibleSpace: FlexibleSpaceBar(
        //  background: 
      ),
      
    );
  }
}
