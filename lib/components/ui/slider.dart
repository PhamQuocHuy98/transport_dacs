import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/components/widgets/dot.dart';
import 'package:dacs/components/widgets/pagewidget.dart';
import 'package:dacs/data/fakedata.dart';
import 'package:dacs/data/local/shared_pref_until.dart';

import 'package:dacs/screens/home/Home.dart';
import 'package:dacs/screens/home/Main.dart';
import 'package:dacs/screens/login_signup/login.dart';

import 'package:dacs/untils/untils.dart';

import 'package:flutter/material.dart';

class IntroSlider extends StatefulWidget {
  @override
  _IntroSliderState createState() => _IntroSliderState();
}

class _IntroSliderState extends State<IntroSlider> {
  int index = 0;
  SharePrefUtil share = new SharePrefUtil();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: Dimension.getHeight(0.75),
          child: PageView.builder(
            onPageChanged: (index) {
              setState(() {
                this.index = index;
              });
            },
            scrollDirection: Axis.horizontal,
            itemCount: lstSplash.length,
            itemBuilder: (context, index) {
              return PageWidget(
                images: lstSplash[index].image,
                subtitle: lstSplash[index].subtitle,
                title: lstSplash[index].title,
              );
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Dot(
              select: index == 0 ? true : false,
            ),
            SizedBox(
              width: 4,
            ),
            Dot(
              select: index == 1 ? true : false,
            ),
            SizedBox(
              width: 4,
            ),
            Dot(
              select: index == 2 ? true : false,
            )
          ],
        ),
        SizedBox(
          height: 30,
        ),
        CommonButton(
          title: 'BẮT ĐẦU',
          startColor: Color.fromRGBO(15, 115, 238, 1.0),
          endColor: Color.fromRGBO(198, 68, 252, 1.0),
          onPressed: () {
            share.getUserId().then((token) => {
                  print('Token la ' + token.toString()),
                  token == null || token == -1
                      ? Navigator.pushNamed(context, "/login")
                      : Navigator.pushNamed(context, "/main")
                });
          },
        ),
        // SizedBox(height: 30),
        // Center(
        //   child: InkWell(
        //     onTap: () {
        //       share.saveToken('token');
        //     },
        //     child: Text(
        //       'Login',
        //       style: StylesText.style20GrayMontserratRegular,
        //     ),
        //   ),
        // )
      ],
    );
  }
}
