import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class TileShimmerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(4.0),
        child: Stack(children: <Widget>[
          Shimmer.fromColors(
            baseColor: Colors.white,
            highlightColor: Colors.grey,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(child: TileShimmer()),
                    Expanded(child: TileShimmer()),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(child: TileShimmer()),
                    Expanded(child: TileShimmer()),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(child: TileShimmer()),
                    Expanded(child: TileShimmer()),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(child: TileShimmer()),
                    Expanded(child: TileShimmer()),
                  ],
                ),
              ],
            ),
          ),
          Shimmer.fromColors(
            baseColor: Colors.white,
            highlightColor: Colors.black38,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                    Expanded(
                        child: TileShimmer(
                      showTitle: true,
                    )),
                  ],
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

class TileShimmer extends StatelessWidget {
  TileShimmer({this.showTitle = false});
  final bool showTitle;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: LayoutBuilder(
            builder: (context, constrants) {
              return Container(
                height: constrants.maxWidth,
                color: showTitle ? Colors.transparent : Colors.white,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      bottom: 10,
                      left: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: constrants.maxWidth * 2 / 3,
                            height: 10,
                            color:
                                showTitle ? Colors.white : Colors.transparent,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 6),
                            child: Container(
                              width: constrants.maxWidth * 1 / 3,
                              height: 10,
                              color:
                                  showTitle ? Colors.white : Colors.transparent,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
