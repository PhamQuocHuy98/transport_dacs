import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';
import 'package:avatar_glow/avatar_glow.dart';

class StepperRoom extends StatelessWidget {
  final int currentIndex;
  final List<String> titles;
  final List<IconData> icons;

  StepperRoom({
    Key key,
    this.currentIndex,
    this.titles = const ['Vị trí', 'Thông tin', 'Phòng', 'Xác nhận'],
    this.icons = const [
      Icons.location_on,
      Icons.info,
      Icons.home,
      Icons.done,
    ],
  }) : //assert(icons.length == titles.length),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Color _renderBgColor(int index) {
      Color bgColor = Colors.white;
      if (index < currentIndex) {
        bgColor = Colors.blue; //Color.fromRGBO(76, 185, 6, 1.0); 
      } else if (index > currentIndex) {
        bgColor = Color.fromRGBO(212, 220, 225, 1.0);
      }
      return bgColor;
    }

    Color _renderIconColor(int index) {
      Color bgColor = Colors.blue;//Color.fromRGBO(76, 185, 6, 1.0);
      return currentIndex == index ? bgColor : Colors.white;
    }

    Widget _containerIcon(int index) {
      IconData icon = icons[index];
      return index != icons.length - 1
          ? Expanded(
              child: Row(
                children: <Widget>[
                  Container(
                    height: 44.0,
                    width: 44.0,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _renderBgColor(index),
                        border: Border.all(
                            width: 2.0,
                            color: index == currentIndex
                                ? Colors.blue//Color.fromRGBO(76, 185, 6, 1.0)
                                : Colors.transparent)),
                    child: Icon(icon, color: _renderIconColor(index)),
                  ),
                  Expanded(
                    child: Container(
                        height: 2.0,
                        color: index < currentIndex
                            ? Colors.blue//Color.fromRGBO(76, 185, 6, 1.0)
                            : Color.fromRGBO(183, 196, 203, 1.0)),
                  )
                ],
              ),
            )
          : Container(
              height: 44.0,
              width: 44.0,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _renderBgColor(index),
                  border: Border.all(
                      color: index == currentIndex
                          ? Colors.blue//Color.fromRGBO(76, 185, 6, 1.0)
                          : Colors.transparent)),
              child: Icon(icon, color: _renderIconColor(index)),
            );
    }

    Widget _renderTitle(int index) {
      String title = titles[index];
      return Container(
        width: 80.0,
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(
              fontSize: 12.0,
              color: Colors.black,
              fontFamily: 'VarelaRound-Regular'),
        ),
      );
    }

    Widget _biggestCircle(int index) {
      return AvatarGlow(
        startDelay: Duration(milliseconds: 1000),
        glowColor: index != currentIndex ? Colors.white : Colors.transparent,
        endRadius: 40.0,
        duration: Duration(milliseconds: 2000),
        repeat: true,
        showTwoGlows: false,
        repeatPauseDuration: Duration(milliseconds: 100),
        child: Container(
          
          height: 64.0,
          width: 64.0,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                  color: Color.fromRGBO(212, 220, 225, 1.0), width: 1.5)),
        ),
      );
    }

    return Container(
        margin: EdgeInsets.only(top: 20.0),
        width: Dimension.getWidth(0.96),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List<Widget>.generate(titles.length, _renderTitle),
              ),
            ),
            Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:
                        List<Widget>.generate(titles.length, _biggestCircle),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  margin: EdgeInsets.symmetric(horizontal: 8.0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      border: Border.all(
                          color: Color.fromRGBO(212, 220, 225, 1.0))),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 18.0),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:
                        List<Widget>.generate(icons.length, _containerIcon),
                  ),
                ),
              ],
            )
          ],
        ));
  }
}
