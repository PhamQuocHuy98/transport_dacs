import 'package:dacs/blocs/uploadroombloc/bloc_infomation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuildInfo extends StatefulWidget {
  @override
  _BuildInfoState createState() => _BuildInfoState();
}

class _BuildInfoState extends State<BuildInfo> {
  @override
  Widget build(BuildContext context) {
    final info = Provider.of<InfomationBloc>(context);
    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 30.0),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            alignment: Alignment.centerLeft,
            child: Text(
              'Thông tin phòng',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            margin: EdgeInsets.only(top: 20.0),
            alignment: Alignment.centerLeft,
            child: Text('Loại phòng'),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                groupValue: int.parse(info.getCheckInfoRoom().toString()),
                value: 1,
                activeColor: Colors.blue,
                onChanged: (int value) {
                  info.setCheckInfoRoom(value);
                },
              ),
              Text('Phòng cho thuê'),
            ],
          ),
          Divider(
            height: 1.0,
            // indent: 50.0,
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                groupValue: int.parse(info.getCheckInfoRoom().toString()),
                value: 2,
                activeColor: Colors.blue,
                onChanged: (int value) {
                  info.setCheckInfoRoom(value);
                },
              ),
              Text('Phòng ở ghép'),
            ],
          ),
          Divider(
            height: 1.0,
            //indent: 50.0,
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text('SỨC CHỨA'),
                ),
                TextField(
                  onChanged: (text) {
                    info.setCapacity(int.parse(text));
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: info.getCapacity() == null
                        ? 'Nhập số người/phòng'
                        : info.getCapacity().toString(),
                  ),
                  keyboardType: TextInputType.number,
                ),
                Divider(
                  color: info.getCapacity() == null ? Colors.red : Colors.grey,
                  height: 1.0,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      info.getCapacity() == null
                          ? 'Vui lòng nhập sức chứa'
                          : '',
                      style: TextStyle(
                        color: info.getCapacity() == null
                            ? Colors.red
                            : Colors.grey,
                      )),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            margin: EdgeInsets.only(top: 20.0),
            alignment: Alignment.centerLeft,
            child: Text('Giới tính'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                groupValue: int.parse(info.getSex().toString()),
                value: 0,
                activeColor: Colors.blue,
                onChanged: (int value) {
                  info.setSex(value);
                },
              ),
              Text('Tất cả'),
            ],
          ),
          Divider(
            height: 1.0,
            //indent: 50.0,
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                groupValue: int.parse(info.getSex().toString()),
                value: 1,
                activeColor: Colors.blue,
                onChanged: (int value) {
                  info.setSex(value);
                },
              ),
              Text('Nam'),
            ],
          ),
          Divider(
            height: 1.0,
            indent: 50.0,
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Radio(
                groupValue: int.parse(info.getSex().toString()),
                value: 2,
                activeColor: Colors.blue,
                onChanged: (int value) {
                  info.setSex(value);
                },
              ),
              Text('Nữ'),
            ],
          ),
          Divider(
            height: 1.0,
            // indent: 50.0,
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text('CHI PHÍ'),
                ),
                TextField(
                  onChanged: (text) {
                    info.setPrice(double.parse(text));
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: info.getPrice() == null
                        ? 'Nhập giá cho thuê'
                        : info.getPrice().toString(),
                  ),
                  keyboardType: TextInputType.number,
                ),
                Divider(
                  color: info.getPrice() == null ? Colors.red : Colors.grey,
                  height: 1.0,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      info.getPrice() == null ? 'Vui lòng nhập sức chứa' : '',
                      style: TextStyle(
                        color:
                            info.getPrice() == null ? Colors.red : Colors.grey,
                      )),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
