import 'package:dacs/blocs/uploadroombloc/bloc_done.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuildDone extends StatefulWidget {
  @override
  _BuildDoneState createState() => _BuildDoneState();
}

class _BuildDoneState extends State<BuildDone> {
  @override
  Widget build(BuildContext context) {
    final done = Provider.of<DoneBloc>(context);
    return Container(
      //width: 100,
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              'Xác nhận thông tin',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                child: Text('SỐ ĐIỆN THOẠI'),
              ),
              TextField(
                onChanged: (text) {
                  done.setPhone(text);
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: done.getPhone() == null
                      ? 'Nhập số điện thoại của bạn'
                      : done.getPhone(),
                ),
                keyboardType: TextInputType.number,
              ),
              Divider(
                height: 1.0,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                child: Text('TIÊU ĐỀ'),
              ),
              TextField(
                onChanged: (text) {
                  done.setTitle(text);
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: done.getTitle() == null
                      ? 'Tiêu đề bài đăng'
                      : done.getTitle(),
                ),
              ),
              Divider(
                height: 1.0,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                child: Text('MÔ TẢ'),
              ),
              TextField(
                onChanged: (text) {
                  done.setDescription(text);
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: done.getDescription() == null
                      ? 'Giá rẻ, môi trường sống sạch đẹp, khu phố văn minh'
                      : done.getDescription(),
                  hintMaxLines: 1,
                ),
              ),
              Divider(
                height: 1.0,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
