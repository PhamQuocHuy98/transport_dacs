import 'package:dacs/styles/styles.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';

class ItemLooking extends StatelessWidget {
  final Color startColor;
  final Color endColor;

  final String image;
  final String title;

  ItemLooking({this.endColor, this.startColor, this.image, this.title});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10.0,
      child: Container(
        width: Dimension.getWidth(0.4),
        height: Dimension.getHeight(0.25),
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            gradient: LinearGradient(
              colors: [
                startColor,
                endColor,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              image,
              fit: BoxFit.contain,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: StylesText.style24WhiteMontserratBold,
            ),
          ],
        ),
      ),
    );
  }
}
