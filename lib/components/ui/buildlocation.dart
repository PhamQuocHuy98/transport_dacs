import 'package:dacs/api/db_api.dart';
import 'package:dacs/blocs/uploadroombloc/bloc_location.dart';
import 'package:dacs/components/widgets/button.dart';
import 'package:dacs/models/district.dart';
import 'package:dacs/models/province.dart';
import 'package:dacs/models/ward.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuildLocaTion extends StatefulWidget {
  BuildLocaTion({
    Key key,
  }) : super(key: key);

  _BuildLocaTionState createState() => _BuildLocaTionState();
}

class _BuildLocaTionState extends State<BuildLocaTion> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final location = Provider.of<LocationBloc>(context);
    return Container(
      //width: 100,
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Địa chỉ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              InkWell(
                onTap: () {},
                child: Text(
                  'Sử dụng vị trí hiện tại',
                  style: TextStyle(color: Colors.blue, fontSize: 15.0),
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  child: FutureBuilder<List<Province>>(
                    future: api.fetchAllProvince(),
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Province>> snapshot) {
                      if (snapshot.hasData) {
                        return AlertDialog(
                          content: Container(
                            width: 100,
                            height: 200,
                            child: ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: const EdgeInsets.only(top: 8.0),
                                  child: CheckButton(
                                    onPressed: () {
                                      // nếu thay đổi TP thì update lại quận /phường
                                      setState(() {
                                        location.setProvince(
                                            snapshot.data[index].name,
                                            snapshot.data[index].id.toString());
                                        location.setDistrict('', '');
                                        location.setWard('', '');
                                      });

                                      Navigator.pop(context);
                                    },
                                    title: snapshot.data[index].name,
                                    width: 70,
                                    color: Colors.grey,
                                    height: 40,
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                      } else {
                        return AlertDialog(
                          content: Container(
                            width: 100,
                            height: 200,
                            child: Center(child: CircularProgressIndicator()),
                          ),
                        );
                      }
                    },
                  ));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text('THÀNH PHỐ'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      location.getProvince(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    InkWell(
                        onTap: () {}, child: Icon(Icons.keyboard_arrow_down))
                  ],
                ),
                Divider(
                  color:
                      location.getProvince() == '' ? Colors.red : Colors.grey,
                  height: 1.0,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      location.getProvince() == ''
                          ? 'Vui lòng chọn thành phố'
                          : '',
                      style: TextStyle(
                        color: location.getProvince() == ''
                            ? Colors.red
                            : Colors.grey,
                      )),
                )
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  child: FutureBuilder<List<District>>(
                    future: api.fetchDistrictById(location.getProvinceId()),
                    builder: (BuildContext context,
                        AsyncSnapshot<List<District>> snapshot) {
                      if (snapshot.hasData) {
                        return AlertDialog(
                          content: Container(
                            width: 100,
                            height: 200,
                            child: ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: const EdgeInsets.only(top: 8.0),
                                  child: CheckButton(
                                    onPressed: () {
                                      // nếu chọn quận huyện thì set lại phường
                                      setState(() {
                                        location.setDistrict(
                                            snapshot.data[index].name,
                                            snapshot.data[index].id.toString());
                                        location.setWard('', '');
                                      });

                                      Navigator.pop(context);
                                    },
                                    title: snapshot.data[index].name,
                                    width: 70,
                                    color: Colors.grey,
                                    height: 40,
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                      } else {
                        return AlertDialog(
                          content: Container(
                            width: 100,
                            height: 200,
                            child: Center(child: CircularProgressIndicator()),
                          ),
                        );
                      }
                    },
                  ));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text('QUẬN/HUYỆN'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      location.getDistrict(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    InkWell(
                        onTap: () {}, child: Icon(Icons.keyboard_arrow_down))
                  ],
                ),
                Divider(
                  color:
                      location.getDistrict() == '' ? Colors.red : Colors.grey,
                  height: 1.0,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      location.getDistrict() == ''
                          ? 'Vui lòng chọn quận huyện'
                          : '',
                      style: TextStyle(
                        color: location.getDistrict() == ''
                            ? Colors.red
                            : Colors.grey,
                      )),
                )
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  child: FutureBuilder<List<Ward>>(
                    future: api.fetchWardById(location.getDistrictId()),
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Ward>> snapshot) {
                      if (snapshot.hasData) {
                        return AlertDialog(
                          content: Container(
                            width: 100,
                            height: 200,
                            child: ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: const EdgeInsets.only(top: 8.0),
                                  child: CheckButton(
                                    onPressed: () {
                                      setState(() {
                                        location.setWard(
                                          snapshot.data[index].name,
                                          snapshot.data[index].id.toString(),
                                        );
                                      });

                                      Navigator.pop(context);
                                    },
                                    title: snapshot.data[index].name,
                                    width: 70,
                                    color: Colors.grey,
                                    height: 40,
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                      } else {
                        return AlertDialog(
                          content: Container(
                            width: 100,
                            height: 200,
                            child: Center(child: CircularProgressIndicator()),
                          ),
                        );
                      }
                    },
                  ));
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text('PHƯỜNG'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      location.getWard(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    InkWell(
                        onTap: () {}, child: Icon(Icons.keyboard_arrow_down))
                  ],
                ),
                Divider(
                  color: location.getWard() == '' ? Colors.red : Colors.grey,
                  height: 1.0,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      location.getWard() == '' ? 'Vui lòng chọn phường' : '',
                      style: TextStyle(
                        color:
                            location.getWard() == '' ? Colors.red : Colors.grey,
                      )),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                child: Text('ĐỊA CHỈ'),
              ),
              TextField(
                onTap: () {
                  print('haha');
                },
                //enabled: false,

                onChanged: (text) {
                  location.setAddress(text);
                },
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: TextStyle(fontWeight: FontWeight.bold),
                    hintText: location.getAddress()),
              ),
              Divider(
                color: location.getAddress() == '' ? Colors.red : Colors.grey,
                height: 1.0,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                    location.getAddress() == '' ? 'Vui lòng nhập địa chỉ' : '',
                    style: TextStyle(
                      color: location.getAddress() == ''
                          ? Colors.red
                          : Colors.grey,
                    )),
              ),
            ],
          )
        ],
      ),
    );
  }
}
