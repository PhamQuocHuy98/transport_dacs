import 'package:dacs/blocs/uploadroombloc/bloc_room.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class BuildRoom extends StatefulWidget {
  BuildRoom({Key key}) : super(key: key);

  _BuildRoomState createState() => _BuildRoomState();
}

class _BuildRoomState extends State<BuildRoom> {
  // List<File> lst;
  @override
  void initState() {
    super.initState();
    // lst = new List<File>();
  }

  @override
  Widget build(BuildContext context) {
    final room = Provider.of<RoomBloc>(context);
    return Container(
      //width: 100,
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              'Đăng tải hình ảnh phòng',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          Container(
            decoration:
                BoxDecoration(border: new Border.all(color: Colors.blueAccent)),
            margin: EdgeInsets.only(top: 20.0),
            width: Dimension.getWidth(0.9),
            height: Dimension.getHeight(0.3),
            // color: Colors.red,
            child: GridView.builder(
              itemCount: room.getLength(), //room.getListRoomImage(),
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
              ),
              itemBuilder: (BuildContext context, int index) {
                return Stack(
                  children: <Widget>[
                    Container(
                      width: 150,
                      height: 150,
                      margin: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: FileImage(
                            room.getItemRoomImage(index),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      // color: Colors.white,
                      width: 150,
                      height: 150,
                      // alignment: Alignment.topRight,
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          //color: Colors.white,
                          width: 40,
                          height: 35,
                          decoration: BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                          child: Center(
                            //padding: const EdgeInsets.all(8.0),
                            child: IconButton(
                              //   alignment: Alignment.center,
                              onPressed: () {
                                room.removeListRoomImage(index);
                                // print(index);
                              },
                              icon: Icon(
                                Icons.close,
                                //size: 10,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                );
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
          RaisedButton(
            color: Colors.grey[300],
            onPressed: () async {
              if (room.getLength() >= 6) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text('Tối đa 6 ảnh'),
                  ),
                );
                return;
              }
              final file = await ImagePicker.pickImage(
                source: ImageSource.gallery,
              );
              if (file != null) {
                room.addListRoomImage(file);
              }
            },
            child: Text(
              'Chọn ảnh',
              style: TextStyle(color: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
