import 'package:dacs/components/widgets/starrating.dart';
import 'package:dacs/untils/untils.dart';
import 'package:flutter/material.dart';

class ItemReviewMessage extends StatelessWidget {
  final int index;
  //final ReviewMessage reviewMessage;

  const ItemReviewMessage({Key key, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('index: $index');
      },
      child: Container(
        width: Dimension.witdh,
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
                width: 1.0, color: Color.fromRGBO(231, 236, 239, 1.0)),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              'Phạm Quốc Huy',
              style: TextStyle(
                fontSize: 14,
                color: Colors.blueAccent,
                //  fontFamily: FontFamily.varelaRoundRegular
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Phòng đẹp sạch thoáng mát',
              style: TextStyle(
                fontSize: 14, //fontFamily: FontFamily.varelaRoundRegular
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  DateTime.now().toString(),
                  style: TextStyle(
                      fontSize: 12,
                      //fontFamily: FontFamily.varelaRoundRegular,
                      color: Color.fromRGBO(117, 134, 146, 1.0)),
                ),
                StarRating(
                  color: Colors.amber,
                  rating: 4,
                  starCount: 5,
                  size: 20,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
