import 'package:dacs/routes/route.dart';
import 'package:dacs/screens/splashs/Splashone.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: RouteGenerator.buildAuthorizedRoutes,
      home: Splash(),
    );
  }
}
